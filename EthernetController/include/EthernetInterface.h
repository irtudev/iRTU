//
// Created by mukeshp on 20-Aug-20.
//

#ifndef ETHERNET_CONTROLLER
#define ETHERNET_CONTROLLER

#include <string.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct ethernetCnfg
{
    char mEnable;
    /* DHCP = 1, Static = 0 */
    char mIsEth0DhcpOrStatic;
    char mEth0IpAddr[20];
    char mEth0NetMask[20];
    char mEth0Gateway[24];
    char mEth0PreferDns[24];
    char mEth0AlterDns[24];
}EthernetCnfg_t;

void ethernet_init_defaults(struct ethernetCnfg* ptr);

int ethernet_setting(void);
void ethernet_start_controller(void) __attribute__ ((noreturn));
int ethernet_connect(void);
int ethernet_disconnect(void);

int Is_Interface_Present(char * ifname);
int Get_Interface_Status(const char * ifname);

extern EthernetCnfg_t gEthernetCnfg;

#endif //IOSERVICE_ETHERNETINTERFACE_H
