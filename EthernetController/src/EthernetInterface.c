//
// Created by mukeshp on 20-Aug-20.
//
#include "EthernetInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <errno.h>

EthernetCnfg_t gEthernetCnfg;

void ethernet_init_defaults(struct ethernetCnfg *ptr)
{
    ptr->mEnable = 1;
    ptr->mIsEth0DhcpOrStatic = 0;
    strcpy(ptr->mEth0IpAddr, "199.199.50.113");
    strcpy(ptr->mEth0NetMask, "255.255.254.0");
    strcpy(ptr->mEth0Gateway, "199.199.50.3");
    strcpy(ptr->mEth0PreferDns, "8.8.8.8");
    strcpy(ptr->mEth0AlterDns, "8.8.4.4");
}

void ethernet_start_controller(void)
{
    int tInterfaceStatus = 0;
    ethernet_setting();
    ethernet_connect();

    while (1)
    {
        sleep(10);
        if(Is_Interface_Present((char *)"eth0"))
        {
            tInterfaceStatus = Get_Interface_Status("eth0");
            if ((tInterfaceStatus & IFF_UP) && (tInterfaceStatus & IFF_RUNNING))
            {
                printf("eth0 interface is up and running\n");
            }
            else
            {
                printf("eth0 interface is not present\n");
                ethernet_disconnect();
                sleep(5);
                ethernet_connect();
            }
        }
    }
}

int ethernet_setting(void)
{
    char tString[512] = {0, };

    if(0 == gEthernetCnfg.mIsEth0DhcpOrStatic) //Static IP
    {
        sprintf(tString, "echo \"auto eth0\n"
                         "iface eth0 inet static\n"
                         "  address %s\n"
                         "  netmask %s\n"
                         "  gateway %s\" > /etc/network/interfaces.d/eth0",
                gEthernetCnfg.mEth0IpAddr, gEthernetCnfg.mEth0NetMask, gEthernetCnfg.mEth0Gateway);
        system(tString);

        sprintf(tString, "echo \"nameserver %s\nnameserver %s\" > /etc/resolv.conf",
                gEthernetCnfg.mEth0PreferDns, gEthernetCnfg.mEth0AlterDns);
        system(tString);
    } // DHCP IP
    else
    {
        sprintf(tString, "echo \"auto eth0\n"
                         "iface eth0 inet dhcp\" > /etc/network/interfaces.d/eth0");
        system(tString);

    }

    return 0;
}

int ethernet_connect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "ifconfig eth0 up");
    system(tCommand);

    return 0;
}

int ethernet_disconnect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "ifconfig eth0 down");
    system(tCommand);

    return 0;
}

int Is_Interface_Present(char * ifname)
{
    struct ifaddrs *ifaddr, *ifa;
    int n=0;

    if(getifaddrs(&ifaddr) == -1)
    {
        printf("Error in getifaddrs at %s (%s)\n",__func__, strerror(errno));
        return -1;
    }

    for(ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
    {
        if(strstr(ifa->ifa_name,ifname) == NULL)
        {
            //required interface found
            freeifaddrs(ifaddr);
            return 1;
        }
    }

    freeifaddrs(ifaddr);
    return 0;
}

int Get_Interface_Status(const char * ifname)
{
    int fd;
    struct ifreq ifr;

    /* AF_INET - to define network interface IPv4*/
    /* Creating soket for it.*/
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        printf("Get_Interface_Status error (%s) at %s\n", strerror(errno), __func__ );
        return 0;
    }

    /*AF_INET - to define IPv4 Address type.*/
    ifr.ifr_addr.sa_family = AF_INET;

    /*eth0 - define the ifr_name - port name where network attached.*/
    memcpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

    /*Accessing network interface information by passing address using ioctl.*/
    if(ioctl(fd, SIOCGIFFLAGS, &ifr) < 0)
    {
        printf("ioctl error (%s) at %s\n", strerror(errno), __func__ );
        close(fd);
        return 0;
    }

    /*closing fd*/
    close(fd);
    if(ifr.ifr_flags)
    {
        return ifr.ifr_flags;
    }

    return 0;
}
