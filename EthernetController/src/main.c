#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "EthernetInterface.h"
#include <RTUCore/Configuration.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Logger.h>

static logger_t* logger = NULL;
static configuration_t* config = NULL;
static void on_signal_received(SignalType stype);

int main(int argc, char**argv)
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    ethernet_init_defaults(&gEthernetCnfg);

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    gEthernetCnfg.mEnable = configuration_get_value_as_char(config, "ethernet", "enable");
    gEthernetCnfg.mIsEth0DhcpOrStatic = configuration_get_value_as_char(config, "ethernet", "mode");
    strncpy(gEthernetCnfg.mEth0IpAddr, configuration_get_value_as_string(config, "ethernet", "ipaddr"), sizeof(gEthernetCnfg.mEth0IpAddr)-1);
    strncpy(gEthernetCnfg.mEth0NetMask, configuration_get_value_as_string(config, "ethernet", "netmask"), sizeof(gEthernetCnfg.mEth0NetMask)-1);
    strncpy(gEthernetCnfg.mEth0Gateway, configuration_get_value_as_string(config, "ethernet", "gateway"), sizeof(gEthernetCnfg.mEth0Gateway)-1);
    strncpy(gEthernetCnfg.mEth0PreferDns, configuration_get_value_as_string(config, "ethernet", "preferDns"), sizeof(gEthernetCnfg.mEth0PreferDns)-1);
    strncpy(gEthernetCnfg.mEth0AlterDns, configuration_get_value_as_string(config, "ethernet", "AlterDns"), sizeof(gEthernetCnfg.mEth0AlterDns)-1);

    if(gEthernetCnfg.mEnable == '1')
    {
        ethernet_start_controller();
    }
    else
    {
        ethernet_disconnect();
    }

    while (1) {sleep(30);}
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
