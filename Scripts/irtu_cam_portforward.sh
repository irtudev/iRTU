ifconfig eth1 192.168.2.100
#iptables -t nat -A PREROUTING -i tun1 -p tcp --dport tun1 -j DNAT --to-destination 192.168.2.101:80
iptables -t nat -A PREROUTING -i tun1 -p tcp --dport 554 -j DNAT --to-destination 192.168.2.101:554
iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
iptables -P FORWARD ACCEPT
sysctl net.ipv4.ip_forward=1
iptables-save
