#!/bin/bash
gpio_pin=50
gpio_sys_class="/sys/class/gpio"
 gpio_export_file="${gpio_sys_class}/export"
 gpio_direction_file="${gpio_sys_class}/gpio${gpio_pin}/direction"
 gpio_value_file="${gpio_sys_class}/gpio${gpio_pin}/value"
sleep_delay=5
 echo ${gpio_pin} > ${gpio_export_file} 2>/dev/null
echo "out" > ${gpio_direction_file}
 while [ 1 ]
  do
    echo 0 > ${gpio_value_file} && sleep ${sleep_delay}
    echo 1 > ${gpio_value_file} && sleep ${sleep_delay}
  done
 #EOF
