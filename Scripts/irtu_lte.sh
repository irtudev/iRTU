#!/bin/bash

echo 146 > /sys/class/gpio/export
echo 147 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio146/direction
echo out > /sys/class/gpio/gpio147/direction
sleep 10
echo 0 > /sys/class/gpio/gpio146/value
sleep 10
echo 0 > /sys/class/gpio/gpio147/value
sleep 10

/sbin/modprobe -v ppp-async
pppd call provider
sleep 10
/sbin/modprobe -r ppp-async
