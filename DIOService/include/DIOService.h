#ifndef DIGITAL_IO_SERVICE
#define DIGITAL_IO_SERVICE

#include <stdbool.h>

typedef struct DigitalIOPin
{
    int pin_no;
    char pin_name[33];
    char pin_type;
}DigitalIOPin;

typedef struct DigitalIOConfiguration
{
    long pin_count;
    DigitalIOPin* pins;
}DigitalIOConfiguration;

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

extern DigitalIOConfiguration digitalIOConfiguration;

#endif
