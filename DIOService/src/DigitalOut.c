#include "DigitalOut.h"

#define BUFFER_MAX      (5)
#define DIRECTION_MAX   (35)
#define VALUE_MAX       (30)

#define IN      (0)
#define OUT     (1)

#define LOW     (0)
#define HIGH    (1)

#define PIN  24 /* P1-18 */
#define POUT 4  /* P1-07 */

static int GPIOExport(logger_t* logger, int pin);
static int GPIOUnexport(logger_t* logger, int pin);
static int GPIODirection(logger_t* logger, int pin, int dir);

static unsigned char digital_out_pin_map[] =
                    {
                        DO1_GPIO_MAP, DO2_GPIO_MAP, DO3_GPIO_MAP, DO4_GPIO_MAP,
                        DO5_GPIO_MAP, DO6_GPIO_MAP, DO7_GPIO_MAP, DO8_GPIO_MAP
                    };

static char error_string[64] = {0};

int digital_out_initialize(logger_t *logger)
{
    for(int index = 0 ; index < MAX_DO ; ++index)
    {
        if (0 == GPIOExport(logger, digital_out_pin_map[index]) )
        {
            if (-1 == GPIODirection(logger, digital_out_pin_map[index], OUT))
            {
                memset(error_string, 0, 64);
                sprintf(error_string, "Error at GPIODirection DO(%d)", index+1);
                WriteLog(logger, error_string, LOG_ERROR);
            }
        }
        else
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Error at GPIOExport DO(%d)", index+1);
            WriteLog(logger, error_string, LOG_ERROR);
        }
    }

    if (0 == GPIOExport(logger, DO_EN_GPIO_MAP) )
    {
        if (-1 == GPIODirection(logger, DO_EN_GPIO_MAP, OUT))
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Error at GPIODirection DO_EN");
            WriteLog(logger, error_string, LOG_ERROR);
        }
    }
    else
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at GPIOExport DO_EN");
        WriteLog(logger, error_string, LOG_ERROR);
    }

    digital_out_write(logger, DO_EN_GPIO_MAP, 0);

    return 0;
}

int digital_out_release(logger_t *logger)
{
    for(int index = 0 ; index < MAX_DO ; ++index)
    {
        if (-1 == GPIOUnexport(logger, digital_out_pin_map[index]) )
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Error at GPIOUnexport DO(%d)", index+1);
            WriteLog(logger, error_string, LOG_ERROR);
        }
    }

    if (-1 == GPIOUnexport(logger, DO_EN_GPIO_MAP) )
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at GPIOUnexport DO_EN");
        WriteLog(logger, error_string, LOG_ERROR);
    }

    return 0;
}

int digital_out_write(logger_t *logger, int pin, int value)
{
    static const char s_values_str[] = "01";

    char path[VALUE_MAX];
    int fd;

    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);

    fd = open(path, O_WRONLY);

    if (-1 == fd)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to open gpio value for writing");
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1))
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to write value");
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    close(fd);

    return(0);
}

static int GPIOExport(logger_t *logger, int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/export", O_WRONLY);

    if (-1 == fd)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to open export for writing");
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIOUnexport(logger_t* logger, int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (-1 == fd)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to open unexport for writing");
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIODirection(logger_t* logger, int pin, int dir)
{
    static const char s_directions_str[]  = "in\0out";
    char path[DIRECTION_MAX];
    int fd;

    snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to open(%s) gpio direction for writing", path);
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3))
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Failed to set direction");
        WriteLog(logger, error_string, LOG_ERROR);
        return(-1);
    }

    close(fd);

    return(0);
}
