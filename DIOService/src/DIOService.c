#include "DIOService.h"
#include "DigitalIn.h"
#include "DigitalOut.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/StringList.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Buffer.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);
static void read_digital_io_configuration(void);
static void read_send_all(const char* node_name);
static void read_send(const char *node_name, int pin_no);

static configuration_t* config;
static message_bus_t* message_bus;
static logger_t* logger;

static char** destination_list = NULL;
static long sampling_rate = 10;
DigitalIOConfiguration digitalIOConfiguration;

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    char value[257] = {0};

    memset(value, 0, 257);
    strcpy(value, configuration_get_value_as_string(config, "default", "destination"));
    WriteInformation(logger, value);
    destination_list = strsplitchar(value, ',');
    sampling_rate = configuration_get_value_as_integer(config, "default", "sampling_rate");
    WriteInformation(logger, configuration_get_value_as_string(config, "default", "sampling_rate"));
    digitalIOConfiguration.pin_count = configuration_get_value_as_integer(config, "default", "pin_count");

    if(!sampling_rate)
    {
        sampling_rate = 600;
    }

    read_digital_io_configuration();
    configuration_release(config);

    return true;
}

void read_digital_io_configuration(void)
{
    digitalIOConfiguration.pins = (DigitalIOPin*)calloc((unsigned)digitalIOConfiguration.pin_count, sizeof (DigitalIOPin));
    long index = 0;
    for(index = 0; index < digitalIOConfiguration.pin_count; index++)
    {
        char section[16] = {0};
        sprintf(section, "pin%ld", index+1);
        if(configuration_has_section(config, section))
        {
            WriteInformation(logger, section);
            digitalIOConfiguration.pins[index].pin_type = configuration_get_value_as_char(config, section, "pin_type");
            digitalIOConfiguration.pins[index].pin_no = (int)configuration_get_value_as_integer(config, section, "pin_no");
            strncpy(digitalIOConfiguration.pins[index].pin_name, configuration_get_value_as_string(config, section, "pin_name"), 32);
            WriteInformation(logger, configuration_get_value_as_string(config, section, "pin_type"));
            WriteInformation(logger, configuration_get_value_as_string(config, section, "pin_no"));
            WriteInformation(logger, digitalIOConfiguration.pins[index].pin_name);
        }
    }
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);
    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    digital_in_initialize(logger);
    digital_out_initialize(logger);

    while(true)
    {
        for(int ctr = 0; destination_list[ctr] != NULL; ctr++)
        {
            read_send_all(destination_list[ctr]);
        }

        sleep((unsigned int)sampling_rate);
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    char error_message[64];

    memset(error_message, 0, sizeof (error_message));
    digital_in_release(logger);

    memset(error_message, 0, sizeof (error_message));
    digital_out_release(logger);

    message_bus_close(message_bus);
    message_bus_release(message_bus);
    strfreelist(destination_list);

    if(digitalIOConfiguration.pins)
    {
        free(digitalIOConfiguration.pins);
    }

    logger_release(logger);

    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char** command_lines = strsplitchar(messagebuffer, '\n');

        char* command_name = command_lines[0];
        char* command_string = command_lines[1];

        if(strstr(command_name, "WRITE"))
        {
            char** command_values = strsplitchar(command_string, ',');;

            for(int index = 0; command_values[index] != NULL; index++)
            {
                // String is composed as PIN:SIGNAL
                unsigned char pin = command_values[index][0];
                unsigned char signal = command_values[index][2];

                digital_out_write(logger, pin, signal);
            }

            strfreelist(command_values);
        }

        if(strstr(command_name, "READ"))
        {
            if(!command_string)
            {
                read_send_all(node_name);
            }
            else
            {
                char** command_values = strsplitchar(command_string, ',');;

                for(int index = 0; command_values[index] != NULL; index++)
                {
                    // String is composed as PIN number only
                    unsigned char pin = (unsigned char)command_values[index][0];
                    read_send(node_name, pin);
                }

                strfreelist(command_values);
            }
        }

        strfreelist(command_lines);
    }
}

void read_send_all(const char *node_name)
{
    buffer_t* payload = buffer_allocate_default();

    // Read from all configured pins
    for(long index = 0;  index < digitalIOConfiguration.pin_count; index++)
    {
        if(digitalIOConfiguration.pins[index].pin_type == 'I')
        {
            int pin_no = digitalIOConfiguration.pins[index].pin_no;
            char* pin_name = digitalIOConfiguration.pins[index].pin_name;

            int pin_value = digital_in_read(logger, pin_no);

            if(pin_value == -1)
            {
                WriteLog(logger, "Could not read", LOG_ERROR);
            }

            buffer_append_string(payload, pin_name);
            buffer_append_char(payload, ':');
            buffer_append_integer(payload, pin_value);
            buffer_append_char(payload, ',');
        }
    }

    buffer_remove_end(payload, 1);

    if(buffer_get_size(payload) > 0)
    {
        WriteInformation(logger, buffer_get_data(payload));

        if(!message_bus_send(message_bus, node_name, Data, Text, (char*)buffer_get_data(payload), (long)buffer_get_size(payload)))
        {
            WriteLog(logger, "IPC send failure", LOG_ERROR);
        }
    }

    buffer_free(payload);
}

void read_send(const char* node_name, int pin_no)
{
    char pin_type = 0;

    for(int index = 0; index < digitalIOConfiguration.pin_count; index++)
    {
        if(digitalIOConfiguration.pins[index].pin_no == pin_no)
        {
            pin_type = digitalIOConfiguration.pins[index].pin_type;
        }
    }

    if(pin_type != 'I')
    {
        return;
    }

    buffer_t* payload = buffer_allocate_default();
    char* pin_name = digitalIOConfiguration.pins[pin_no].pin_name;
    int pin_value = digital_in_read(logger, pin_no);
    buffer_append_string(payload, pin_name);
    buffer_append_char(payload, ':');
    buffer_append_integer(payload, pin_value);
    message_bus_send(message_bus, node_name, Data, Text, (char*)buffer_get_data(payload), (long)buffer_get_size(payload));
    WriteInformation(logger, buffer_get_data(payload));
    buffer_free(payload);
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
