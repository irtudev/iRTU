#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "Logger.h"
#include "SignalHandler.h"
#include "StringEx.h"
#include "Directory.h"
#include "Configuration.h"
#include "Environment.h"
#include "Buffer.h"
#include "MessageBus.h"

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType stype);
static logger_t* logger = NULL;

int main(int argc, char* argv[])
{
    system("clear");

    buffer_t* buffer_ptr = buffer_allocate_default();

    buffer_append_string(buffer_ptr, "Hello World");
    buffer_append_char(buffer_ptr, ' ');
    buffer_append_real(buffer_ptr, 66.67);
    buffer_append_char(buffer_ptr, ' ');
    buffer_append_integer(buffer_ptr, 1111);
    buffer_append_char(buffer_ptr, ' ');
    buffer_append_boolean(buffer_ptr, false);
    buffer_append_char(buffer_ptr, ' ');
    buffer_append_curr_timestamp(buffer_ptr);

    const char* temp = buffer_get_data(buffer_ptr);
    printf("%s\n", temp);
    buffer_free(buffer_ptr);

    char* proc_name = (char*)calloc(1, 128);
    proc_name = env_get_current_process_name(proc_name);
    printf("%s\n", proc_name);
    free(proc_name);

    printf("Allocating logger\n");
    logger = logger_allocate_default();
    printf("Logger file is %s\n", logger_filename(logger));

    if(logger == NULL)
    {
        printf("*** ERROR could not allocate logger\n");
        return -1;
    }

    logger_enable_console_out(logger, true);

    WriteInformation(logger, "Logger Initialized");

    WriteInformation(logger, "Setting signal handler");

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    WriteInformation(logger, "Testing function name handling");

    WriteInformation(logger, "Allocating configuration");

    configuration_t* conf = configuration_allocate_default();
    printf("Configuration file is %s\n", configuration_filename(conf));

    WriteInformation(logger, "Reading all configuration sections");
    char** sections = configuration_get_all_sections(conf);

    for(int sindex = 0; sections[sindex] != 0; sindex++)
    {
        char* sec_str = NULL;
        sec_str = sections[sindex];
        printf("Section %s\n", sec_str);

        printf("Reading all keys of configuration section %s\n", sections[sindex]);
        char** keys = configuration_get_all_keys(conf, sections[sindex]);

        for(int kindex = 0; keys[kindex] != 0; kindex++)
        {
            char* key_str = NULL;
            key_str = keys[kindex];
            printf("Key %s Value %s\n", key_str, configuration_get_value_as_string(conf, sec_str, key_str));
        }

        strfreelist(keys);
    }

    strfreelist(sections);

    WriteInformation(logger, "Releasing configuration");
    configuration_release(conf);

    message_bus_t* mb = message_bus_initialize(on_network_event);

    if(!mb)
    {
        printf("Could not open message bus");
    }

    message_bus_open(mb);

    message_bus_send(mb, "rtucoretest", Data, Text, "Hello", 5);

    while(true)
    {
        sleep(5);
    }

    message_bus_release(mb);

    WriteInformation(logger, "Releasing logger");
    logger_release(logger);

    return 0;
}

static void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_WARNING);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_WARNING);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            logger_release(logger);
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_WARNING);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_WARNING);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_WARNING);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "TERMINAL WINDOW RESIZED SIGNAL", LOG_WARNING);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_WARNING);
            break;
        }
    }
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    long debug_point = 0;
}

