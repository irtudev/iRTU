/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "Dictionary.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct key_value_t
{
    void* key;
    void* value;
    size_t value_size;
    size_t key_size;
    struct key_value_t* next;
}key_value_t;

typedef struct hash_bucket_t
{
    unsigned long hash;
    long key_value_count;
    key_value_t* key_value_list;
    struct hash_bucket_t* next;
}hash_bucket_t;

typedef struct dictionary_t
{
    long hash_count;
    hash_bucket_t* hash_bucket;
}dictionary_t;

void dictionary_internal_add_hash_bucket(dictionary_t* dict_ptr, const void* key, const size_t key_size);
void dictionary_internal_add_key_value(dictionary_t* dict_ptr, const unsigned long hash, const void *key, size_t key_size, const void* value, const size_t value_size);
unsigned long dictionary_internal_get_hash(const void* key, const size_t key_size);

dictionary_t* dictionary_allocate()
{
    dictionary_t* ptr = (dictionary_t*)calloc(1, sizeof (dictionary_t));
    ptr->hash_bucket = NULL;
    ptr->hash_count = 0;
    return ptr;
}

void dictionary_free(dictionary_t* dict_ptr)
{
    if(dict_ptr == NULL)
    {
        return;
    }

    hash_bucket_t* temp_hash_bucket = NULL;
    hash_bucket_t* head_hash_bucket = dict_ptr->hash_bucket;

    while(head_hash_bucket != NULL)
    {
        temp_hash_bucket = head_hash_bucket;

        key_value_t* temp_kv = NULL;
        key_value_t* head_kv = temp_hash_bucket->key_value_list;

        while(head_kv != NULL)
        {
            temp_kv = head_kv;
            head_kv = head_kv->next;
            free(temp_kv->key);
            free(temp_kv->value);
            free(temp_kv);
        }

        free(temp_hash_bucket);

        head_hash_bucket = head_hash_bucket->next;
    }

    free(dict_ptr);
}

void dictionary_set_value(dictionary_t *dict_ptr, const void* key, const size_t key_size, const void* value, const size_t value_size)
{
    if(dict_ptr == NULL)
    {
        return;
    }

    unsigned long current_hash = dictionary_internal_get_hash(key, key_size);

    hash_bucket_t* current_hash_bucket = dict_ptr->hash_bucket;

    while(current_hash_bucket != NULL)
    {
        if(current_hash_bucket->hash == current_hash)
        {
            key_value_t* current_kv = current_hash_bucket->key_value_list;

            while(current_kv)
            {
                if(memcmp(current_kv->key, key, key_size) == 0)
                {
                    free(current_kv->value);
                    current_kv->value = calloc(value_size, sizeof(char));
                    memcpy(current_kv->value, value, value_size);
                    return;
                }

                current_kv = current_kv->next;
            }

            dictionary_internal_add_key_value(dict_ptr, current_hash, key, key_size, value, value_size);
        }

        current_hash_bucket = current_hash_bucket->next;
    }

    dictionary_internal_add_hash_bucket(dict_ptr, key, key_size);
    dictionary_internal_add_key_value(dict_ptr, current_hash, key, key_size, value, value_size);

    return;
}

void* dictionary_get_value(dictionary_t* dict_ptr, const void *key, const size_t key_size)
{
    if(dict_ptr == NULL)
    {
        return NULL;
    }

    unsigned long current_hash = dictionary_internal_get_hash(key, key_size);

    hash_bucket_t* current_hash_bucket = dict_ptr->hash_bucket;

    while(current_hash_bucket != NULL)
    {
        if(current_hash_bucket->hash == current_hash)
        {
            key_value_t* current_kv = current_hash_bucket->key_value_list;

            while(current_kv)
            {
                if(memcmp(current_kv->key, key, key_size) == 0)
                {
                    return current_kv->value;
                }

                current_kv = current_kv->next;
            }
        }

        current_hash_bucket = current_hash_bucket->next;
    }

    return NULL;
}

char** dictionary_get_all_keys(dictionary_t* dict_ptr)
{
    if(dict_ptr == NULL)
    {
        return NULL;
    }

    long total_keys = 0;
    hash_bucket_t* current_hash_bucket = NULL;
    long index = 0;

    current_hash_bucket = dict_ptr->hash_bucket;
    while( current_hash_bucket != NULL)
    {
        total_keys = total_keys + current_hash_bucket->key_value_count;
        current_hash_bucket = current_hash_bucket->next;
    }

    char** buffer = NULL;
    buffer = (char **)calloc(1, (unsigned long)(total_keys + 1) * sizeof(char*));

    current_hash_bucket = dict_ptr->hash_bucket;
    while( current_hash_bucket != NULL)
    {
        key_value_t* current_kv = current_hash_bucket->key_value_list;

        while(current_kv)
        {
            buffer[index] = (char*)calloc(current_kv->key_size+1, sizeof(char));
            memcpy(buffer[index], current_kv->key, current_kv->key_size);
            current_kv = current_kv->next;
            index++;
        }
        current_hash_bucket = current_hash_bucket->next;
    }

    return buffer;
}

void dictionary_free_key_list(dictionary_t* dict_ptr, char **key_list)
{
    if(dict_ptr == NULL)
    {
        return;
    }

    long index = 0;

    while(key_list[index] != NULL)
    {
        free(key_list[index]);
        index++;
    }

    free(key_list);
}

void dictionary_internal_add_hash_bucket(dictionary_t *dict_ptr, const void *key, const size_t key_size)
{
    if(dict_ptr == NULL)
    {
        return;
    }

    hash_bucket_t* new_hash_bucket = (hash_bucket_t*)calloc(1, sizeof(hash_bucket_t));
    new_hash_bucket->next = NULL;
    new_hash_bucket->key_value_list = NULL;
    new_hash_bucket->key_value_count = 0;
    new_hash_bucket->hash = dictionary_internal_get_hash(key, key_size);

    if(dict_ptr->hash_bucket == NULL)
    {
        dict_ptr->hash_bucket = new_hash_bucket;
    }
    else
    {
        hash_bucket_t* temp = NULL;
        for(temp = dict_ptr->hash_bucket; temp->next != NULL; temp = temp->next) {}
        temp->next = new_hash_bucket;
    }

    dict_ptr->hash_count++;
}

void dictionary_internal_add_key_value(dictionary_t *dict_ptr, const unsigned long hash, const void* key, size_t key_size, const void *value, const size_t value_size)
{
    if(dict_ptr == NULL)
    {
        return;
    }

    hash_bucket_t* curr_hash_bucket = NULL;

    for(curr_hash_bucket = dict_ptr->hash_bucket; curr_hash_bucket != NULL; curr_hash_bucket = curr_hash_bucket->next)
    {
        if(curr_hash_bucket->hash ==  hash)
        {
            key_value_t* new_kv = (key_value_t*)calloc(1, sizeof (key_value_t));
            new_kv->next = NULL;
            new_kv->key = (char*)calloc(1, key_size);
            memcpy(new_kv->key, key, key_size);
            new_kv->value = (char*)calloc(1, value_size);
            memcpy(new_kv->value, value, value_size);
            new_kv->value_size = value_size;
            new_kv->key_size = key_size;

            if(curr_hash_bucket->key_value_list == NULL)
            {
                curr_hash_bucket->key_value_list = new_kv;
            }
            else
            {
                key_value_t* temp = NULL;
                for(temp = curr_hash_bucket->key_value_list; temp->next != NULL; temp = temp->next) {}
                temp->next = new_kv;
            }

            curr_hash_bucket->key_value_count++;

            break;
        }
    }

}

unsigned long dictionary_internal_get_hash(const void *key, const size_t key_size)
{
    unsigned long hash = 0;
    size_t index = 0;

    if (!key)
        return 0;

    const char* key_buffer = (const char*)key;

    for(hash = 0, index = 0 ; index < key_size ; index++)
    {
        hash += (unsigned)key_buffer[index] ;
        hash += (hash<<10);
        hash ^= (hash>>6) ;
    }

    hash += (hash <<3);
    hash ^= (hash >>11);
    hash += (hash <<15);

    return hash;
}
