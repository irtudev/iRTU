/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef MESSAGE_BUS
#define MESSAGE_BUS

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))
#define LIBRARY_ENTRY __attribute__((constructor))
#define LIBRARY_EXIT __attribute__((destructor))

// Shared libary load/unload handlers
extern  LIBRARY_ENTRY void library_load();
extern  LIBRARY_EXIT void library_unload();

// Enumerations
typedef enum PayloadType
{
    Data='D',
    Event='E',
    Request='Q',
    Response='R'
}PayloadType;

typedef enum DataType
{
    Text='T',
    Video='V',
    Image='I',
    Audio='A',
    Raw='R'
}DataType;

typedef struct message_bus_t message_bus_t;

typedef void(*messabus_bus_callback)(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);

extern LIBRARY_EXPORT message_bus_t* message_bus_initialize(messabus_bus_callback callback);
extern LIBRARY_EXPORT bool message_bus_release(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_open(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_close(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_send(message_bus_t* ptr, const char* node_name, PayloadType ptype, DataType dtype, char* messagebuffer, long buffersize);

#ifdef __cplusplus
}
#endif

#endif

