/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef ENVIRONMENT_C
#define ENVIRONMENT_C

#include <string.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

extern LIBRARY_EXPORT char*   env_get_current_process_name(char* ptr);
extern LIBRARY_EXPORT char*   env_get_current_user_name();
extern LIBRARY_EXPORT char*   env_get_lock_filename();
extern LIBRARY_EXPORT bool    env_lock_process(const char* lock_filename);

#ifdef __cplusplus
}
#endif

#endif
