/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef CONFIGURATION_INI
#define CONFIGURATION_INI

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

typedef struct configuration_t configuration_t;

extern LIBRARY_EXPORT configuration_t* configuration_allocate_default();
extern LIBRARY_EXPORT configuration_t* configuration_allocate(const char* filename);
extern LIBRARY_EXPORT void  configuration_release(configuration_t* config);
extern LIBRARY_EXPORT const char* configuration_filename(configuration_t* config);

extern LIBRARY_EXPORT char**  configuration_get_all_sections(const configuration_t* config);
extern LIBRARY_EXPORT char**  configuration_get_all_keys(const configuration_t* config, const char* section);

extern LIBRARY_EXPORT bool  configuration_has_section(const configuration_t* config, const char* section);
extern LIBRARY_EXPORT bool  configuration_has_key(const configuration_t* config, const char* section, char* key);

extern LIBRARY_EXPORT long  configuration_get_value_as_integer(const configuration_t* config, const char* section, const char* key);
extern LIBRARY_EXPORT bool  configuration_get_value_as_boolean(const configuration_t* config, const char* section, const char* key);
extern LIBRARY_EXPORT double configuration_get_value_as_real(const configuration_t* config, const char* section, const char* key);
extern LIBRARY_EXPORT const char *configuration_get_value_as_string(const configuration_t* config, const char* section, const char* key);
extern LIBRARY_EXPORT char configuration_get_value_as_char(const configuration_t* config, const char* section, const char* key);

#ifdef __cplusplus
}
#endif

#endif
