/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef STRING_LIST_C
#define STRING_LIST_C

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

typedef struct string_list_t string_list_t;

extern LIBRARY_EXPORT string_list_t* str_list_allocate(string_list_t* lptr);
extern LIBRARY_EXPORT string_list_t* str_list_allocate_from_string(string_list_t* lptr, const char* str, const char* delimiter);
extern LIBRARY_EXPORT void str_list_clear(string_list_t* lptr);
extern LIBRARY_EXPORT void str_list_free(string_list_t* lptr);

extern LIBRARY_EXPORT void str_list_lock(string_list_t* lptr);
extern LIBRARY_EXPORT void str_list_unlock(string_list_t* lptr);

extern LIBRARY_EXPORT void str_list_add(string_list_t* lptr, char* data);
extern LIBRARY_EXPORT void str_list_insert(string_list_t* lptr, char* data, long pos);

extern LIBRARY_EXPORT void str_list_remove(string_list_t* lptr, const char* data);
extern LIBRARY_EXPORT void str_list_remove_at(string_list_t* lptr, long pos);

extern LIBRARY_EXPORT long str_list_item_count(string_list_t* lptr);
extern LIBRARY_EXPORT long str_list_index_of(string_list_t* lptr, const char* data);
extern LIBRARY_EXPORT long str_list_index_of_like(string_list_t* lptr, const char* data);
extern LIBRARY_EXPORT char* str_list_get_at(string_list_t* lptr, long atpos);

extern LIBRARY_EXPORT char* str_list_get_first(string_list_t* lptr);
extern LIBRARY_EXPORT char* str_list_get_next(string_list_t* lptr);
extern LIBRARY_EXPORT char* str_list_get_last(string_list_t* lptr);
extern LIBRARY_EXPORT char* str_list_get_previous(string_list_t* lptr);

extern LIBRARY_EXPORT void str_list_sort(string_list_t* lptr);
extern LIBRARY_EXPORT void str_list_merge(string_list_t* lptrFirst, string_list_t* lptrSecond);
extern LIBRARY_EXPORT void str_list_join(string_list_t* lptrFirst, string_list_t* lptrSecond);

#ifdef __cplusplus
}
#endif

#endif
