#ifndef PROTOCOL_MQTT
#define PROTOCOL_MQTT

#include "IProtocolINET.h"

bool mqtt_initialize(logger_t* loggerptr);
bool mqtt_connect(const char* str_server, int num_port, const char* deviceid, downlink_callback dlink);
bool mqtt_send_data(const char* str, const char* uri);
bool mqtt_send_response(const char* str, const char* uri);
bool mqtt_send_request(const char* str, const char* uri);
bool mqtt_send_event(const char* str, const char* uri);
bool mqtt_close();
bool mqtt_release();

#endif

