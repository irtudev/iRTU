#ifndef I_PROTOCOL_INET
#define I_PROTOCOL_INET

#include <RTUCore/Logger.h>

typedef void(*downlink_callback)(const char* messageptr, long len);

static char server[33] = {0};
static int port = 0;
static char device_id[33] = {0};
static char uri[33] = {0};
static downlink_callback downlink;
static logger_t* logger = NULL;

#endif
