#ifndef PROTOCOL_HTTP
#define PROTOCOL_HTTP

#include "IProtocolINET.h"

bool http_initialize(logger_t* loggerptr);
bool http_connect(const char* str_server, int num_port, const char* deviceid, downlink_callback dlink);
bool http_send_data(const char* str, const char* uri);
bool http_send_response(const char* str, const char* uri);
bool http_send_request(const char* str, const char* uri);
bool http_send_event(const char* str, const char* uri);
bool http_close();
bool http_release();
char* http_get_public_ip_address(const char* resolver_url);

#endif
