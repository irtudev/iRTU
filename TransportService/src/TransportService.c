#include "TransportService.h"
#include "ProtocolHTTP.h"
#include "ProtocolMQTT.h"

#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <time.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netdb.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Buffer.h>
#include <RTUCore/Environment.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);
static void on_downlink(const char *messageptr, long len);
static void on_data(const char *nodename, const char* messagebuffer, char dtype);
static void on_event(const char *nodename, const char* messagebuffer, char dtype);
static void on_request(const char *nodename, const char* messagebuffer, char dtype);
static void on_response(const char *nodename, const char* messagebuffer, char dtype);

static int generate_device_id(const char* network_interface, char* device_id);
static int get_ip_address(const char *network_interface, char *ip_address);
static void upload(const char *nodename, const char* messagebuffer, char dtype, const char* mtype);
static void send_device_identity(void);

static message_bus_t* message_bus = NULL;
static configuration_t* config = NULL;
static char version[16] = {0};
static char protocol[16] = {0};
static char default_network_interface[12] = {0};
static char tunnel_network_interface[12] = {0};

static bool enabled = true;
static float latitude = 0;
static float longitude = 0;
static char public_ip_address_resolver[64]= {0};
static char public_ip_address[16] = {0};
static char tunnel_ip_address[16] = {0};

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        printf("Could not open log file %s\n", logger_filename(logger));
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();
    WriteLog(logger, configuration_filename(config), LOG_ERROR);

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    WriteInformation(logger, "Found configuration");

    strcpy(version, configuration_get_value_as_string(config, "default", "version"));
    strcpy(protocol, configuration_get_value_as_string(config, "default", "protocol"));
    strcpy(server, configuration_get_value_as_string(config, "default", "server"));
    strcpy(uri, configuration_get_value_as_string(config, "default", "uri"));
    strcpy(public_ip_address_resolver, configuration_get_value_as_string(config, "default", "public_ip_address_resolver"));
    port = (int)configuration_get_value_as_integer(config, "default", "port");
    enabled = configuration_get_value_as_boolean(config, "default", "enabled");

    strcpy(default_network_interface, configuration_get_value_as_string(config, "default", "device_id_src"));

    WriteInformation(logger, "Probing NIC ");
    WriteInformation(logger, default_network_interface);
    generate_device_id(default_network_interface, device_id);
    WriteInformation(logger, "Device ID is ");
    WriteInformation(logger, device_id);

    strcpy(tunnel_network_interface, configuration_get_value_as_string(config, "default", "tunnel_device"));

    if(strlen(tunnel_network_interface) < 1)
    {
        WriteLog(logger, "No tunnelling network device provided", LOG_ERROR);
    }
    else
    {
        WriteInformation(logger, "Found tunneling device");
        WriteInformation(logger, tunnel_network_interface);
        memset(tunnel_ip_address, 0, 16);
        get_ip_address(tunnel_network_interface, tunnel_ip_address);

        if(strlen(tunnel_ip_address) > 6)
        {
            WriteInformation(logger, tunnel_ip_address);
        }
    }

    configuration_release(config);

    WriteInformation(logger, "Read all configuration keys");

    if(!http_initialize(logger))
    {
        WriteLog(logger, "Could not load HTTP library", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    if(!mqtt_initialize(logger))
    {
        WriteLog(logger, "Could not load MQTT library", LOG_ERROR);
        http_release();
        logger_release(logger);
        return false;
    }

    char* ptr  = NULL;

    ptr  = http_get_public_ip_address(public_ip_address_resolver);

    while(ptr == NULL)
    {
        WriteLog(logger, "Could not connect to internet", LOG_ERROR);
        WriteInformation(logger, "Checking for internet in 30 seconds");
        ptr  = http_get_public_ip_address(public_ip_address_resolver);
        sleep(30);
    }

    strcpy(public_ip_address, ptr);
    free(ptr);

    WriteInformation(logger, "Got public IP address");
    WriteInformation(logger, public_ip_address);

    return true;
}

bool service_start()
{
    if(enabled)
    {
        WriteInformation(logger, "Transport Service is enabled");

        if(strcmp(protocol, "http") == 0)
        {
            if(!http_connect(server, port, device_id, on_downlink))
            {
                return false;
            }
        }

        if(strcmp(protocol, "mqtt") == 0)
        {
            if(!mqtt_connect(server, port, device_id, on_downlink))
            {
                return false;
            }
        }

        WriteInformation(logger, "Initialized protocol adapter");
    }

    message_bus = message_bus_initialize(on_network_event);
    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Initialized message bus client");

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Connected to message bus");

    bool continue_loop = true;

    send_device_identity();

    time_t old_time = 0;
    time_t new_time = 0;
    time_t dif_time = 0;
    time_t run_time = 0;

    time(&old_time);

    while(continue_loop)
    {
        sleep(10);
        new_time = old_time;
        time(&old_time);
        dif_time = old_time - new_time;

        run_time = run_time + dif_time;

        if(run_time >= 3600)
        {
            send_device_identity();
            run_time = 0;
        }
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    if(strcmp(protocol, "http") == 0)
    {
        http_close();
    }

    if(strcmp(protocol, "mqtt") == 0)
    {
        mqtt_close();
    }

    http_release();
    mqtt_release();

    WriteInformation(logger, "Closed and released protocol adapter");

    message_bus_close(message_bus);
    WriteInformation(logger, "Closed connection to message bus");
    message_bus_release(message_bus);
    WriteInformation(logger, "Released message bus");

    logger_release(logger);

    return false;
}

void on_downlink(const char *messageptr, long len)
{
    bool ret = false;
    char* header = NULL;
    char* destination_payload = NULL;
    char** header_fields = NULL;

    strsplitkeyvaluechar(messageptr, '\n', &header, &destination_payload);
    header_fields = strsplitchar(header, ',');
    free(header);

    int field_count = 0;
    for(field_count = 0; header_fields[field_count] != NULL; field_count++);

    if(field_count != 9)
    {
        strfreelist(header_fields);
        free(header);
        free(destination_payload);
        WriteLog(logger, "Rejecting downlink command due field count mismatch, number of header fields must be 9", LOG_WARNING);
        return;
    }

    PayloadType ptype = Request;
    DataType dtype = Raw;

    char recipient[32] = {0};
    strcpy(recipient, header_fields[4]);

    stralltrim(recipient);

    if(strcmp(header_fields[5], "text") == 0)
    {
        dtype = Text;
    }
    else
    {
        if(strcmp(header_fields[5], "image") == 0)
        {
            dtype = Image;
        }
        else
        {
            if(strcmp(header_fields[5], "audio") == 0)
            {
                dtype = Audio;
            }
            else
            {
                if(strcmp(header_fields[5], "video") == 0)
                {
                    dtype = Video;
                }
                else
                {
                    dtype = Raw;
                }
            }
        }
    }

    strfreelist(header_fields);
    ret =  message_bus_send(message_bus, recipient, ptype, dtype, destination_payload, strlen(destination_payload));
    free(destination_payload);
    return;
}

void on_data(const char *nodename, const char *messagebuffer, char dtype)
{
    upload(nodename, messagebuffer, dtype, "data");
}

void on_event(const char *nodename, const char *messagebuffer, char dtype)
{
    upload(nodename, messagebuffer, dtype, "event");
}

void on_request(const char *nodename, const char *messagebuffer, char dtype)
{
    upload(nodename, messagebuffer, dtype, "request");
}

void on_response(const char *nodename, const char *messagebuffer, char dtype)
{
    upload(nodename, messagebuffer, dtype, "response");
}

void upload(const char *nodename, const char *messagebuffer, char dtype, const char *mtype)
{
    buffer_t* payload = buffer_allocate_default();

    buffer_append_char(payload, '{');

    if(dtype == Image || dtype == Audio || dtype == Video)
    {
        buffer_append_string(payload, "\"encoding\":\"base64\",");
    }

    if(dtype == Text)
    {
        buffer_append_string(payload, "\"encoding\":\"ascii\",");
    }

    buffer_append_string(payload, "\"transport\":\"");
    buffer_append_string(payload, protocol);
    buffer_append_string(payload, "\",");
    buffer_append_string(payload, "\"device_id\":\"");
    buffer_append_string(payload, device_id);
    buffer_append_string(payload, "\",");
    buffer_append_string(payload, "\"sender\":\"");
    buffer_append_string(payload, nodename);
    buffer_append_string(payload, "\",");

    switch(dtype)
    {
        case Text:
        {
            buffer_append_string(payload, "\"type\":\"text\",");
            break;
        }
        case Image:
        {
            buffer_append_string(payload, "\"type\":\"image\",");
            break;
        }
        case Audio:
        {
            buffer_append_string(payload, "\"type\":\"audio\",");
            break;
        }
        case Video:
        {
            buffer_append_string(payload, "\"type\":\"video\",");
            break;
        }
        case Raw:
        default:
        {
            break;
        }
    }

    buffer_append_string(payload, "\"version\":\"");
    buffer_append_string(payload, version);
    buffer_append_string(payload, "\",");
    buffer_append_string(payload, "\"payload_type\":\"");
    buffer_append_string(payload, mtype);
    buffer_append_string(payload, "\",");
    buffer_append_string(payload, "\"longitude\":");
    buffer_append_real(payload, longitude);
    buffer_append_string(payload, ",");
    buffer_append_string(payload, "\"latitude\":");
    buffer_append_real(payload, latitude);
    buffer_append_string(payload, ",");
    buffer_append_string(payload, "\"payload_id\":");
    buffer_append_string(payload, "0,");
    buffer_append_string(payload, "\"timestamp\":");
    buffer_append_curr_timestamp(payload);
    buffer_append_string(payload, ",");

    buffer_append_string(payload, "\"payload\":{");
    buffer_append_string(payload, messagebuffer);
    buffer_append_char(payload, '}');
    buffer_append_char(payload, '}');

    if(strcmp(mtype, "event") == 0)
    {
        if(strcmp(protocol, "http") == 0)
        {
            http_send_event(buffer_get_data(payload), uri);
        }

        if(strcmp(protocol, "mqtt") == 0)
        {
            mqtt_send_event(buffer_get_data(payload), uri);
        }
    }

    if(strcmp(mtype, "data") == 0)
    {
        if(strcmp(protocol, "http") == 0)
        {
            http_send_data(buffer_get_data(payload), uri);
        }

        if(strcmp(protocol, "mqtt") == 0)
        {
            mqtt_send_data(buffer_get_data(payload), uri);
        }
    }

    if(strcmp(mtype, "request") == 0)
    {
        if(strcmp(protocol, "http") == 0)
        {
            http_send_request(buffer_get_data(payload), uri);
        }

        if(strcmp(protocol, "mqtt") == 0)
        {
            mqtt_send_request(buffer_get_data(payload), uri);
        }
    }

    if(strcmp(mtype, "response") == 0)
    {
        if(strcmp(protocol, "http") == 0)
        {
            http_send_response(buffer_get_data(payload), uri);
        }

        if(strcmp(protocol, "mqtt") == 0)
        {
            mqtt_send_response(buffer_get_data(payload), uri);
        }
    }

    buffer_free(payload);
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    char buffer[64] = {0};

    if(ptype == Data)
    {
        on_data(node_name, messagebuffer, dtype);
    }

    if(ptype == Response)
    {
        on_response(node_name, messagebuffer, dtype);
    }

    if(ptype == Request)
    {
        on_request(node_name, messagebuffer, dtype);
    }

    if(ptype == Event)
    {
        on_event(node_name, messagebuffer, dtype);
    }
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_WARNING);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_WARNING);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_WARNING);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_WARNING);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_WARNING);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZED CHANGED SIGNAL", LOG_WARNING);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_WARNING);
            break;
        }
    }
}

void send_device_identity(void)
{
    char process_name[64] = {0};
    env_get_current_process_name(process_name);
    buffer_t* buffer = buffer_allocate_default();

    buffer_append_string(buffer, "\"device_id\":\"");
    buffer_append_string(buffer, device_id);
    buffer_append_char(buffer, '"');

    if(strlen(public_ip_address) > 6)
    {
        buffer_append_char(buffer, ',');
        buffer_append_string(buffer, "\"public_ip_address\":\"");
        buffer_append_string(buffer, public_ip_address);
        buffer_append_char(buffer, '"');
    }

    configuration_t* cnf = configuration_allocate("/etc/location.conf");

    if(cnf)
    {
        const char* lon = configuration_get_value_as_string(cnf, "default", "longitude");
        const char* lat = configuration_get_value_as_string(cnf, "default", "latitude");

        longitude = configuration_get_value_as_real(cnf, "default", "longitude");
        latitude = configuration_get_value_as_real(cnf, "default", "latitude");

        buffer_append_char(buffer, ',');
        buffer_append_string(buffer, "\"longitude\":");
        buffer_append_string(buffer, lon);
        buffer_append_char(buffer, ',');
        buffer_append_string(buffer, "\"latitude\":");
        buffer_append_string(buffer, lat);
        configuration_release(cnf);
    }

    if(strlen(tunnel_network_interface) > 1)
    {
        memset(tunnel_ip_address, 0, 16);
        get_ip_address(tunnel_network_interface, tunnel_ip_address);

        if(strlen(tunnel_ip_address) > 6)
        {
            buffer_append_char(buffer, ',');
            buffer_append_string(buffer, "\"tunnel_ip_address\":\"");
            buffer_append_string(buffer, tunnel_ip_address);
            buffer_append_char(buffer, '"');
        }
    }

    upload(process_name, buffer_get_data(buffer), Text, "event");
    buffer_free(buffer);
    WriteInformation(logger, "Sent device identification");
}

int generate_device_id(const char *network_interface, char *device_id)
{
    struct ifaddrs *ifaddr= NULL;
    struct ifaddrs *ifa = NULL;
    int i = 0;

    if (getifaddrs(&ifaddr) == -1)
    {
         perror("getifaddrs");
    }
    else
    {
         for ( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
         {
             if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
             {
                  struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;

                  if(strcmp(network_interface, ifa->ifa_name) == 0)
                  {
                      for (i=0; i <s->sll_halen; i++)
                      {
                          char temp[3] = {0};
                          sprintf(temp, "%02x", (s->sll_addr[i]));
                          strcat(device_id, temp);
                      }

                      for(int idx = 0; device_id[idx] != 0; idx++)
                      {
                            if(isalpha(device_id[idx]))
                            {
                                device_id[idx] = toupper(device_id[idx]);
                            }
                      }

                      freeifaddrs(ifaddr);

                      return 0;
                  }
             }
         }
         freeifaddrs(ifaddr);
    }

    return -1;
}

int get_ip_address(const char *network_interface, char *ip_address)
{
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;

    strncpy(ifr.ifr_name , network_interface , IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);

    sprintf(ip_address, "%s" , inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr) );

    return 0;
}
