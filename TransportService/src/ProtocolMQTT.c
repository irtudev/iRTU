#include "ProtocolMQTT.h"
#include <stdio.h>
#include <mosquitto.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

static bool run = true;
static char client_id[33] = {0};
static int keep_alive = 60;
static struct mosquitto *mosq = NULL;
static pthread_t thread = 0;

static void on_connect(struct mosquitto *mosq, void *obj, int result);
static void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
static void* receiver_thread(void* ptr);
static bool mqtt_internal_send(const char* topic, int data_len, const char* data);

bool mqtt_initialize(logger_t* loggerptr)
{
    int rc = 0;

    rc = mosquitto_lib_init();
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(loggerptr, "Could not initialize MQTT stack", LOG_ERROR);
        return false;
    }

    logger = loggerptr;

    return true;
}

bool mqtt_connect(const char* str_server, int num_port, const char* deviceid, downlink_callback dlink)
{
    int rc = 0;
    run = true;
    port = num_port;
    keep_alive = 60;
    strcpy(server, str_server);
    strcpy(device_id, deviceid);
    strcpy(client_id, deviceid);
    downlink = dlink;
    bool clean_session = true;

    mosq = mosquitto_new(NULL, clean_session, NULL);

    if(!mosq)
    {
        WriteLog(logger, "Could not create a MQTT client", LOG_ERROR);
        return false;
    }

    mosquitto_message_callback_set(mosq, on_message);

    rc = mosquitto_connect(mosq, server, port, keep_alive);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger, "Could not connect to MQTT broker", LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Connected to MQTT broker");

    pthread_attr_t pthread_attr;
    memset(&pthread_attr, 0, sizeof(pthread_attr_t));
    // default threading attributes
    pthread_attr_init(&pthread_attr);
    // allow a thread to exit cleanly without a join
    pthread_attr_setdetachstate (&pthread_attr,PTHREAD_CREATE_DETACHED);
    if (pthread_create(&thread, &pthread_attr, receiver_thread, NULL) != 0)
    {
        return false;
    }

    pthread_attr_destroy(&pthread_attr);

    //Cloud / Web can send us request, response and data only
    //So we will subscribe for those topics

    char subscription_topic[33];

    memset(subscription_topic, 0, 33);
    strcat(subscription_topic, device_id);
    strcat(subscription_topic, "/request/+");
    rc = mosquitto_subscribe(mosq, NULL, subscription_topic, 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger, "Could not subscribe", LOG_ERROR);
        WriteLog(logger, subscription_topic, LOG_ERROR);
        return false;
    }

    memset(subscription_topic, 0, 33);
    strcat(subscription_topic, device_id);
    strcat(subscription_topic, "/response/+");
    rc = mosquitto_subscribe(mosq, NULL, subscription_topic, 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger, "Could not subscribe", LOG_ERROR);
        WriteLog(logger, subscription_topic, LOG_ERROR);
        return false;
    }

    memset(subscription_topic, 0, 33);
    strcat(subscription_topic, device_id);
    strcat(subscription_topic, "/data/+");
    rc = mosquitto_subscribe(mosq, NULL, subscription_topic, 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger, "Could not subscribe", LOG_ERROR);
        WriteLog(logger, subscription_topic, LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Subscribed all topics");

    return true;
}

bool mqtt_close()
{
    run = false;
    sleep(4);

    int rc = mosquitto_disconnect(mosq);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger, "Could not disconnect from MQTT broker", LOG_ERROR);
        return false;
    }

    mosquitto_destroy(mosq);
    return true;
}

bool mqtt_release()
{
    mosquitto_lib_cleanup();
    return true;
}

bool mqtt_send_data(const char* str, const char* uri)
{
    char publish_topic[32] = {0};
    sprintf(publish_topic, "%s/data/", uri);
    int data_len = (int)strlen(str);

    return mqtt_internal_send(publish_topic, data_len, str);
}

bool mqtt_send_response(const char* str, const char* uri)
{
    char publish_topic[32] = {0};
    sprintf(publish_topic, "%s/response/", uri);
    int data_len = (int)strlen(str);

    return mqtt_internal_send(publish_topic, data_len, str);
}

bool mqtt_send_request(const char* str, const char* uri)
{   
    char publish_topic[32] = {0};
    sprintf(publish_topic, "%s/request/", uri);
    int data_len = (int)strlen(str);

    return mqtt_internal_send(publish_topic, data_len, str);
}

bool mqtt_send_event(const char* str, const char* uri)
{
    char publish_topic[32] = {0};
    sprintf(publish_topic, "%s/event/", uri);
    int data_len = (int)strlen(str);

    return mqtt_internal_send(publish_topic, data_len, str);
}

bool mqtt_internal_send(const char* topic, int data_len, const char* data)
{
    if(mosquitto_publish(mosq, NULL, topic, data_len, data, 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

static void* receiver_thread(void *ptr)
{
    int rc = 0;

    while(run)
    {
        rc = mosquitto_loop(mosq, -1, 1);

        if(run && rc)
        {
            WriteLog(logger, "MQTT connection error ... reconnecting in 10 seconds", LOG_ERROR);
            sleep(2);

            rc = mosquitto_reconnect(mosq);

            if(rc != MOSQ_ERR_SUCCESS)
            {
                WriteLog(logger, "MQTT ould not reconnect", LOG_ERROR);
            }
        }
    }

    if(ptr)
    {
        return ptr;
    }
    else
    {
        return NULL;
    }
}

void on_connect(struct mosquitto *mosq, void *obj, int result)
{
    WriteLog(logger, "MQTT connect callback", LOG_INFO);
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    if(strstr(message->topic, device_id) && (
       strstr(message->topic, "/request/") ||
       strstr(message->topic, "/response/") ||
       strstr(message->topic, "/data/")
     ))
    {
        if(downlink)
        {
            downlink((char*)message->payload, message->payloadlen);
        }
    }
    else
    {
        WriteLog(logger, "Unsubscribed message received, ignored", LOG_WARNING);
    }
}



