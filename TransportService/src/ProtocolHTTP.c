#include "ProtocolHTTP.h"
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>

static CURL *curl = NULL;

static bool http_internal_send(const char* str, const char* url_buffer);

bool http_initialize(logger_t* loggerptr)
{
    int rc = 0;
    curl_global_init(CURL_GLOBAL_ALL);

    if(rc != CURLE_OK)
    {
        WriteLog(loggerptr, "Could not initialize MQTT stack", LOG_ERROR);
        return false;
    }

    logger = loggerptr;

    return true;
}

bool http_connect(const char* str_server, int num_port, const char* deviceid, downlink_callback dlink)
{
    port = num_port;
    strcpy(server, str_server);
    strcpy(device_id, deviceid);
    downlink = dlink;
    return true;
}

bool http_close()
{
    return true;
}

bool http_release()
{
    curl_global_cleanup();
    return true;
}

bool http_send_data(const char* str, const char* uri)
{
    char url_buffer[1024] = {0};
    sprintf(url_buffer, "http://%s:%d/%s/data/", server, port, uri);

    return http_internal_send(str, url_buffer);
}

bool http_send_response(const char* str, const char* uri)
{
    char url_buffer[1024] = {0};
    sprintf(url_buffer, "http://%s:%d/%s/response/", server, port, uri);

    return http_internal_send(str, url_buffer);
}

bool http_send_request(const char* str, const char* uri)
{
    char url_buffer[1024] = {0};
    sprintf(url_buffer, "http://%s:%d/%s/request/", server, port, uri);

    return http_internal_send(str, url_buffer);
}

bool http_send_event(const char* str, const char* uri)
{
    char url_buffer[1024] = {0};
    sprintf(url_buffer, "http://%s:%d/%s/event/", server, port, uri);

    return http_internal_send(str, url_buffer);
}

bool http_internal_send(const char* str, const char* url_buffer)
{
    bool ret = true;
    int res = CURLE_OK;

    curl = curl_easy_init();

    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url_buffer);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK)
        {
            ret = false;
        }
    }

    if(curl)
    {
        curl_easy_cleanup(curl);
    }

    return ret;
}

struct memory {
   char *response;
   size_t size;
 };

static size_t cb(void *data, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct memory *mem = (struct memory *)userp;

  char *ptr = realloc(mem->response, mem->size + realsize + 1);
  if(ptr == NULL)
    return 0;  /* out of memory! */

  mem->response = ptr;
  memcpy(&(mem->response[mem->size]), data, realsize);
  mem->size += realsize;
  mem->response[mem->size] = 0;

  return realsize;
}

char* http_get_public_ip_address(const char *resolver_url)
{
    struct memory chunk;
    memset(&chunk, 0, sizeof(struct memory));
    char* ip_address = NULL;

    int res = CURLE_OK;

    curl = curl_easy_init();

    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, resolver_url);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

        res = curl_easy_perform(curl);

        if(res == CURLE_OK)
        {
            ip_address = (char*)calloc(1, strlen(chunk.response)+1);
            strcpy(ip_address, chunk.response);
        }

        if(chunk.response)
        {
            free(chunk.response);
        }
    }

    curl_easy_cleanup(curl);

    return ip_address;
}
