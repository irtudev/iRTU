#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <RTUCore/Logger.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/MessageBus.h>
#include <RTUCore/StringEx.h>

#if !defined(bool)
#define bool int
#define false 0
#define true 1
#endif

#if !defined(NULL)
#define NULL 0
#endif


#define MAX_CPU_BUFF_DATA_LEN 255
#define MAX_CPU_TIME_LEN 10

int GetCpuTotalTime();
int ReadFirstThreeValue();
float AvgCpuUses();
void FanOn();
void FanOff();
void InitializeFan();
float ReadCpuDieTemp();
static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);

static logger_t* logger = NULL;
static message_bus_t* message_bus = NULL;
static bool ipc_open = false;

int main()
{
    logger = logger_allocate_default();

    if(!logger)
    {
        return -1;
    }

    configuration_t* config = NULL;
    config = configuration_allocate_default();

    if(!config)
    {
        logger_release(logger);
        return -1;
    }

    float safe_temperature_threshold = configuration_get_value_as_real(config, "default", "safe_temperature_threshold");

    configuration_release(config);

    message_bus = message_bus_initialize(on_network_event);

    if(message_bus)
    {
        if(message_bus_open(message_bus))
        {
            ipc_open = true;
        }
    }

    float cpudietemp = -1;
    float CpuUses = -1;
    int fanstatus = 0;

    InitializeFan();

    while(1)
    {
        cpudietemp = ReadCpuDieTemp();
        CpuUses = AvgCpuUses();
        char buffer[64] = {0};
        if(cpudietemp > safe_temperature_threshold)
        {
            FanOn();
            fanstatus = 1;
            sprintf(buffer, "Fan torned ON, CPU Temperature:%f, CPU Usage:%f", cpudietemp, CpuUses);
            WriteInformation(logger, buffer);
            if(ipc_open)
            {
                message_bus_send(message_bus, "service_transport", Event, Text, buffer, strlen(buffer));
            }
        }

        if(cpudietemp < safe_temperature_threshold)
        {
            FanOff();
            fanstatus = 0;
            sprintf(buffer, "Fan turned OFF, CPU Temperature:%f, CPU Usage:%f", cpudietemp, CpuUses);
            WriteInformation(logger, buffer);
            if(ipc_open)
            {
                message_bus_send(message_bus, "service_transport", Event, Text, buffer, strlen(buffer));
            }
        }

        sleep(2);
    }
}

//https://stackoverflow.com/questions/3017162/how-to-get-total-cpu-usage-in-linux-using-c

//Add all values cpu  132140 624 28990 1991476 23293 0 4427 0 0 0
int GetCpuTotalTime()
{
    char str[255] = {0};
    const char d[2] = " ";
    char* token;
    long int sum = 0;
    FILE *fstat = fopen("/proc/stat", "r");

    if (fstat == NULL)
    {
        printf("%d: %s --> Failed to open %s\n", __LINE__, __func__,"/proc/stat");
        return -1;
    }

    fgets(str,100,fstat);
    //printf("str = %s\n",str);
    fclose(fstat);
    token = strtok(str,d);

    while(token!=NULL)
    {
      token = strtok(NULL,d);
      if(token!=NULL)
      {
        sum += atoi(token);
        //printf("token = %s sum = %d\n",token,sum);
      }
    }
    return sum;
}

//read first 3 values of cpu  132140 624 28990 1991476 23293 0 4427 0 0 0
int ReadFirstThreeValue()
{
    int i = 1;
    char str[255] = {0};
    const char d[2] = " ";
    char* token;
    long int thardsum = 0;
    FILE *fstat = fopen("/proc/stat", "r");
    if (fstat == NULL) {
        printf("%d: %s --> Failed to open %s\n", __LINE__, __func__,"/proc/stat");
        return -1;
    }
    fgets(str,100,fstat);
    //printf("str = %s\n",str);
    fclose(fstat);
    token = strtok(str,d);
    while(token!=NULL)
    {
      token = strtok(NULL,d);
      if(token!=NULL)
      {
        thardsum += atoi(token);
        //printf("2-token = %s thardsum = %d\n",token,thardsum);
        if(i == 3)
        {
          return thardsum;
        }
      }
      i++;
    }
    return -1;
}

float AvgCpuUses()
{
    long int sum = 0, idle, lastSum = 0,lastIdle = 0, dsum = 0, didle = 0;
    lastSum = GetCpuTotalTime();
    lastIdle = ReadFirstThreeValue();
    //printf("1-cpu uses = %f\n",((float)lastIdle/lastSum)*100);
    sleep(1);
    sum = GetCpuTotalTime();
    idle = ReadFirstThreeValue();
    //printf("idle = %d lastIdle = %d sum = %d lastSum = %d\n",idle,lastIdle,sum,lastSum);
    dsum = (sum-lastSum);
    didle = (idle-lastIdle);
    //printf("didle = %d\n",didle);
    //printf("dSum = %d\n",dsum);
    //printf("devide = %f\n",(float)didle/dsum);
    return (((float)didle/dsum)*100);
}

void FanOn()
{
  char commands[100];
  sprintf((char *)commands,"echo \"1\" > /sys/class/gpio/gpio112/value");
  system((const char *)commands);
}

void FanOff()
{
    char commands[100];
    sprintf((char *)commands,"echo \"0\" > /sys/class/gpio/gpio112/value");
    system((const char *)commands);
}

void InitializeFan()
{
  int gpiofd;
  char commands[100];
    gpiofd = open("/sys/class/gpio/gpio112/direction", O_RDWR);
    if (gpiofd < 0)
    {
        system("echo \'112\' > /sys/class/gpio/export");
    }
    close(gpiofd);
  sprintf((char *)commands,"echo \"out\" > /sys/class/gpio/gpio112/direction");
  system((const char *)commands);
}

float ReadCpuDieTemp()
{
    char buf[20] = {0};
    int status = -1;
    int fd = -1;
    fd = open("/sys/devices/virtual/thermal/thermal_zone0/temp", O_RDONLY);
    if (fd < 0)
    {
      printf("func = %s line = %d fd %d\n",__func__,__LINE__,fd);
      return -1;
    }
    memset(buf,0,sizeof(buf));
    status = read(fd, buf, 20);
    if(status < 0)
    {
      printf("func = %s line = %d status %d\n",__func__,__LINE__,status);
      return -1;
    }
    //printf("func = %s line = %d status %d buf = %s\n",__func__,__LINE__,status,buf);
    close(fd);
    return atof(buf)/1000;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{

}
