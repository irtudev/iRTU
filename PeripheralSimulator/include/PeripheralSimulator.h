#ifndef PERIPHERAL_SIMULATOR
#define PERIPHERAL_SIMULATOR

#include <stdbool.h>

bool simulator_initialize();
bool simulator_destroy();
bool simulator_start();
bool simulator_restart();
bool simulator_stop();

typedef enum ParameterType
{
    Integer = 'i',
    Real = 'r',
    Boolean = 'b',
    Timestamp = 't',
    String = 's',
    Unknown = 'x'
}ParameterType;

typedef struct SimulationParameter
{
    long long UpperBound;
    long long LowerBound;
    ParameterType Type;
    char ParameterName[33];
}SimulationParameter;

#endif
