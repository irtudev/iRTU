#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "Modem.h"
#include <RTUCore/Configuration.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Logger.h>

static logger_t* logger = NULL;
static configuration_t* config = NULL;
static void on_signal_received(SignalType stype);

int main(int argc, char** argv)
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    modemparaInit(&gModemPara);
    modemCnfgInit(&gModemCnfg[0]);
    modemCnfgInit(&gModemCnfg[2]);

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    gModemPara.mEnable = configuration_get_value_as_char(config, "modem", "enable");
    strncpy(gModemPara.mSerialPort, configuration_get_value_as_string(config, "modem", "serialport"), sizeof(gModemPara.mSerialPort)-1);
    gModemPara.mSerialBaudRate = (int)configuration_get_value_as_integer(config, "modem", "SerialBaudRate");
    strncpy(gModemPara.mApnName, configuration_get_value_as_string(config, "modem", "apn"), sizeof(gModemPara.mApnName)-1);
    strncpy(gModemCnfg[0].mModelId, configuration_get_value_as_string(config, "modem", "model1"), sizeof(gModemPara.mApnName)-1);
    strncpy(gModemCnfg[1].mModelId, configuration_get_value_as_string(config, "modem", "model2"), sizeof(gModemPara.mApnName)-1);
    gModemCnfg[0].mModemType = (int)configuration_get_value_as_integer(config, "modem", "modemType");

    configuration_release(config);

    system("mkdir -p /var/log/rtu/modem");
    system("touch /var/log/rtu/modem/ppp-logfile.log");

    if(gModemPara.mEnable == '1')
    {
        modem_start_controller();
    }

    while (1) {sleep(30);}
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            ModemPowerOff();
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
