#ifndef MODEM_CONTROLLER
#define MODEM_CONTROLLER

#include <stdbool.h>
#include <stdint.h>

#define MAX_SUPPORTED_MODEM     (4)

#define MODEM_TTY_DEVICE "/dev/ttyACM0"

#define PPP_PATH   "/etc/ppp/peers/"
#define DELAYMS (1000)
#define FILEPATH    (255)
#define COMMANDSIZE (64)
#define RESPONSESIZE (128)
#define MAX_APNNAME     32
#define MAX_MODELNAME   32
#define MAX_FILEAME     64
#define MAX_MODELID     32
#define MODEM_RETRY_COUNTER (3)

#define MODEM_WAIT_INTERVAL (3)
#define MODEM_NEXT_INTERVAL (1)

#define MODEM_ON_OFF_GPIO_STR   "gpio101"
#define MODEM_ON_OFF_GPIO       101

#define MODEM_RESET_GPIO_STR   "gpio146"
#define MODEM_RESET_GPIO       146

#define MODEM_4V_ENABLE_GPIO_STR   "gpio147"
#define MODEM_4V_ENABLE_GPIO       147

#define MODEM_STATUS_GPIO_STR   "gpio159"
#define MODEM_STATUS_GPIO       159

#define MODEM_RTS_GPIO_STR   "gpio151"
#define MODEM_RTS_GPIO       151

#define MODEM_DTR_GPIO_STR   "gpio150"
#define MODEM_DTR_GPIO       150

#define MODEM_TYPE_4G       (0)
#define MODEM_TYPE_2G       (1)

enum
{
    MODEM_PIN_CNFG = 0,
    MODEM_POWER_ON,
    MODEM_TTY_DETECT,
    MODEM_DETECT_MODEM_ID,
    MODEM_INIT,
    MODEM_SET_APN,
    MODEM_ACTIVATE_PPP,
    MODEM_CHECK_PPP_STATUS,
    MODEM_POWER_OFF,
    MODEM_WAIT_FOR_STATE,
    MODEM_TROUBLESHOOT,
    MODEM_READ_CONFIG,
    MODEM_IDLE,
    MODEM_MAX
};

typedef struct modempara
{
    char mEnable;
    char mSerialPort[256];
    int  mSerialBaudRate;
    int  mSignal;
    int  mRegistration;
    char mSimstatus[32];
    char mModelno[32];
    char mRevision[32];
    char mManufacturedid[32];
    char mApnName[32];
    char mImei[32];
    char mStatus;
}ModemPara_t;

void modemparaInit(struct modempara* ptr);

typedef struct modemCnfg
{
    char mModemName[MAX_MODELNAME];
    char mModelId[MAX_MODELID];
    char mFileName[MAX_FILEAME];
    int mModemType;
}ModemCnfg_t;

void modemCnfgInit(struct modemCnfg* ptr);

void modem_start_controller(void) __attribute__ ((noreturn));

int GetModemIdTemp(int fd);
int ModemDetectModelID(void);
int ModemNameselect(char *pModelId, char *pModemName);
void ModemGetId(char* pModemId);

void ModemIOPinsConfigure(void);

void ModemPowerOn(void);
void ModemPowerOff(void);
int ModemInitialization(ModemPara_t *pModemPara);
int ModemSetApnNameToChatScript(ModemCnfg_t *pModemCnfg);
int ModemActivatePPPInterface(ModemCnfg_t *pModemCnfg);
int ModemCheckPPPInterfaceStatus(void);
int ModemReadConfigurationfromDB(ModemCnfg_t *pModemCnfg);
int ModemFindTroubleShootParameters(ModemCnfg_t *pModemCnfg);
int ModemTtyInterfaceDetection(void);

int ModemReqResponse(const char *pRequest, char* pResponse, int RespLen, int iDelayus);
int ModemReqResponse2G(const char *pRequest, char* pResponse, int RespLen, int iDelayus);
int GetSet_Apn_Name(const char *pFilePath, const char *oldapnname, const char *pNewApnName);
char ReplaceAPNName(const char *str, const char *keystr, char *apnstr);
int SeperateStringAndFind(char *string, const char *headstr, const char *filterstr, char counterfiter, char *resultstr);
int Is_Interface_Present(char * ifname);
int Get_Interface_Status(const char * ifname);

extern ModemCnfg_t gModemCnfg[MAX_SUPPORTED_MODEM];
extern ModemPara_t gModemPara;
extern int gModemIndex;

#endif
