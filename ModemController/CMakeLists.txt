cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(APP_VERSION_MAJOR 1)
set(APP_VERSION_MINOR 0)
set(APP_VERSION_PATCH 0)

set(Project ModemController)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore)

set(SOURCE
${SOURCE}
./src/Modem.c
./src/SerialInterface.c
./src/main.c
)

set(HEADERS
${HEADERS}
./include/Modem.h
./include/SerialInterface.h
)

set(CONFIG
${CONFIG}
./etc/controller_modem.conf
)

set(CONFIG_PPP
${CONFIG_PPP}
./etc/NL-SW-LTE-TC4EU
./etc/NL-SW-LTE-TC4EU-chat
)

set(LOG
${LOG}
./log/controller_modem.log
)

add_executable(controller_modem ${HEADERS} ${SOURCE})

install(TARGETS controller_modem DESTINATION /usr/bin)
install(FILES ${CONFIG} DESTINATION /etc)
install(FILES ${CONFIG_PPP} DESTINATION /etc/ppp/peers/)
install(FILES ${LOG} DESTINATION /var/log)

set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_PATCH})
set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Subrato Roy")
set(CPACK_PACKAGE_DESCRIPTION, "Modem Control Service")

include(CPack)
