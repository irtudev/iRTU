# iRTU

NOTE: The following steps are tested on debian(buster)/ubuntu(bionic) with armv7l/x86-64 architecture.

## Install dependencies for building services/applications in the repository
```
apt update
apt install mosquitto mosquitto-clients libmosquitto-dev libcurl4-openssl-dev libmodbus-dev cmake git
```

## Clone the iRTU repository

```
git clone https://gitlab.com/irtudev/iRTU.git
```
NOTE: If you are unable to clone the repository due to SSL certificate issue, add your system's public ssh key('~/.ssh/id_ed25519.pub' or '~/.ssh/id_rsa.pub' and begins with 'ssh-ed25519' or 'ssh-rsa') to your gitlab user account (user settings > SSH Keys)

## Steps to build and install RTUCore Library

Go inside RTUCore project in iRTU repository
```
cd iRTU/RTUCore
```

Follow the steps below to build and install RTUCore library on device
```
mkdir build && cd build
cmake ..
make
cpack ..
apt install ./<name of deb package generated>
```

To build any service/application, go to the respective service/application project directory and follow the above steps provided to build and install RTUCore library.