#ifndef OTA_SERVICE
#define OTA_SERVICE

#include <stdbool.h>

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
