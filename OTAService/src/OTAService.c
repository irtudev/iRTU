#include "OTAService.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/StringList.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Dictionary.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);

static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;
static configuration_t* config = NULL;
static char* ansible_file = NULL;

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    char* temp = (char*)configuration_get_value_as_string(config, "default", "ansible_file");

    if(temp)
    {
        ansible_file = (char*)calloc(1, strlen(temp)+1);
        strcpy(ansible_file, temp);
    }

    configuration_release(config);

    if(ansible_file == NULL)
    {
        WriteLog(logger, "Ansible configuration not found", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);

    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    while(true)
    {
        sleep(10);
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    free(ansible_file);
    logger_release(logger);
    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{  
    char str[128] = {0};

    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char** command_lines = strsplitchar(messagebuffer, '\n');

        char* command_name = command_lines[0];
        char* module_name = command_lines[1];

        if(strstr(command_name, "UPDATE"))
        {
            memset(str, 0, 128);
            sprintf(str,"ansible-playbook /etc/OTA_service/service_ota.yaml");
            system(str);
            memset(str, 0, 128);
            sprintf(str,"grep ok /var/log/ansible.log | tail -1 >> /log/service_ota.log");
            system(str);
        }

        strfreelist(command_lines);
    }
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
