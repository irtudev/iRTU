#ifndef PERIPHERAL_SIMULATOR
#define PERIPHERAL_SIMULATOR

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#if !defined(bool)
#define bool int
#define false 0
#define true 1
#endif

#if !defined(NULL)
#define NULL 0
#endif

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
