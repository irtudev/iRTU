#include "BluetoothService.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/StringList.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Dictionary.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#include <sys/socket.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);

static void* message_bus = NULL;
static void* logger = NULL;
static void* config = NULL;
static string_list_t* destination_list = NULL;
static dictionary_t* domain_payloads = NULL;
static char** domains = NULL;

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    char* value = NULL;
    value = (char*)configuration_get_value_as_string(config, "default", "destination");
    destination_list = str_list_allocate_from_string(destination_list, value, ",");

    domains = configuration_get_all_keys(config, "payloads");

    if(domains != NULL)
    {
        domain_payloads = dictionary_allocate();

        for(int index = 0; domains[index] != NULL; index++)
        {
            value = (char*)configuration_get_value_as_string(config, "payloads", domains[index]);
            dictionary_set_value(domain_payloads, domains[index], strlen(domains[index]), value, strlen(value));
        }
    }

    configuration_release(config);

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);

    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    bool continue_loop = true;

    inquiry_info *ii = NULL;
    int max_rsp, num_rsp;
    int dev_id, sock, len, flags;
    int i;
    char addr[19] = { 0 };
    char name[248] = { 0 };

    dev_id = hci_get_route(NULL);
    sock = hci_open_dev( dev_id );
    if (dev_id < 0 || sock < 0) {
        perror("opening socket");
        exit(1);
    }

    len  = 8;
    max_rsp = 255;
    flags = IREQ_CACHE_FLUSH;
    ii = (inquiry_info*)malloc(max_rsp * sizeof(inquiry_info));

    num_rsp = hci_inquiry(dev_id, len, max_rsp, NULL, &ii, flags);
    if( num_rsp < 0 ) perror("hci_inquiry");

    for (i = 0; i < num_rsp; i++) {
        ba2str(&(ii+i)->bdaddr, addr);
        memset(name, 0, sizeof(name));
        if (hci_read_remote_name(sock, &(ii+i)->bdaddr, sizeof(name),
            name, 0) < 0)
        strcpy(name, "[unknown]");
        printf("%s  %s\n", addr, name);
    }

    free( ii );
    close( sock );


    while(continue_loop)
    {
        sleep(10);

        char* destination = str_list_get_first(destination_list);

//        while(destination)
//        {
//            for(int index = 0; domains[index] != NULL; index++)
//            {
//                char* payload = dictionary_get_value(domain_payloads, domains[index], strlen(domains[index]));
//                if(!message_bus_send(message_bus, destination, Data, Text, payload, strlen(payload)))
//                {
//                    continue_loop = false;
//                    break;
//                }
//            }

//            destination = str_list_get_next(destination_list);
//        }
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    str_list_free(destination_list);
    dictionary_free(domain_payloads);
    logger_release(logger);
    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{  
    char str[64] = {0};

    WriteInformation(logger,messagebuffer);
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
