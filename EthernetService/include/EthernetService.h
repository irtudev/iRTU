#ifndef ETHERNET_SERVICE
#define ETHERNET_SERVICE

#include <stdbool.h>
#include <stdint.h>

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
