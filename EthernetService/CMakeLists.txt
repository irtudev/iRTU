cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(Project EthernetService)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore)

set(SOURCE
${SOURCE}
./src/EthernetService.c
./src/main.c
)

set(HEADERS
${HEADERS}
./include/EthernetService.h
)

add_executable(service_ethernet ${SOURCE} ${HEADERS})
