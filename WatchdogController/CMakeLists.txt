cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(Project WatchdogController)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore)

set(SOURCE
${SOURCE}
./src/main.c
)

set(HEADERS
${HEADERS}
)

set(CONFIG
${CONFIG}
./etc/controller_watchdog.conf
)

add_executable(controller_watchdog ${SOURCE} ${HEADERS})

install(TARGETS controller_watchdog DESTINATION /usr/bin)
install(FILES ${CONFIG} DESTINATION /etc)
set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_PATCH})
set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Subrato Roy")
set(CPACK_PACKAGE_DESCRIPTION, "Fan Control Service")
