cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(APP_VERSION_MAJOR 1)
set(APP_VERSION_MINOR 0)
set(APP_VERSION_PATCH 0)

set(Project VPNService)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore)

set(SOURCE
${SOURCE}
./src/VPNService.c
./src/main.c
)

set(HEADERS
${HEADERS}
./include/VPNService.h
)

set(CONFIG
${CONFIG}
./etc/service_vpn.conf
)

add_executable(service_vpn ${SOURCE} ${HEADERS})

install(TARGETS service_vpn DESTINATION /usr/bin)
install(FILES ${CONFIG} DESTINATION /etc)
set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_PATCH})

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Subrato Roy")
set(CPACK_PACKAGE_DESCRIPTION, "VPN Service")

include(CPack)

