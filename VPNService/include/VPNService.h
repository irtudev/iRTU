#ifndef VPN_SERVICE
#define VPN_SERVICE

#include <stdbool.h>

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
