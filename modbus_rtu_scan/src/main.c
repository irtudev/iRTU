#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <memory.h>
#include <modbus/modbus.h>

union endian
{
  float f;
  unsigned int u;
};

float decode_float(const unsigned int val0,const unsigned int val1);
long decode_long(const unsigned int val0,const unsigned int val1);
long hex_to_decimal(char* hexVal);
void print_help(void);

int main(int argc, char* argv[])
{  
    modbus_t *context = NULL;
    char serial_port[65] = {0};

    if(argc != 13)
    {
        print_help();
        return -1;
    }

    strncpy(serial_port, argv[1], 64);
    int baud_rate = atoi(argv[2]);
    char parity = argv[3][0];
    int data_bits = atoi(argv[4]);
    int stop_bits = atoi(argv[5]);
    unsigned int port_timeout_seconds = (unsigned int)atoi(argv[6]);

    int slave_id = atoi(argv[7]);
    int read_type = atoi(argv[8]);
    int start_address = atoi(argv[9]);
    int num_registers = atoi(argv[10]);
    char data_type = argv[11][0];
    float calculation_factor = atof(argv[12]);

    printf("Port %s, Baud rate %d, Parity %c, Data bits %d, Stop bits %d, Port timeout %u\n", serial_port, baud_rate, parity, data_bits, stop_bits, port_timeout_seconds);
    context = modbus_new_rtu(serial_port, baud_rate, parity, data_bits, stop_bits);

    if (NULL == context)
    {
        printf("Could not get a MODBUS context\n");
        return -1;
    }

    modbus_set_response_timeout(context, port_timeout_seconds, 0);

    if (-1 == modbus_connect(context))
    {
        printf("Could not connect to the MODBUS device");
        modbus_free(context);
        return -1;
    }

    void* ptr = NULL;
    int ret = 0;
    unsigned long read_size;

    if(read_type == MODBUS_FC_READ_COILS || read_type == MODBUS_FC_READ_DISCRETE_INPUTS)
    {
        read_size = sizeof(uint8_t)* (unsigned long)num_registers;
    }
    else
    {
        read_size = sizeof(uint16_t)* (unsigned long)num_registers;
    }

    ptr = calloc(1, read_size);

    printf("Read type [Hexadecimal 0x%x Integer %d] , Start address [Hexadecimal 0x%x Integer %d], Number of registers %d, Bytes to be read %ld, Data Type %c\n", read_type, read_type, start_address, start_address, num_registers, read_size, data_type);
    modbus_set_slave(context, slave_id);

    switch(read_type)
    {
        case MODBUS_FC_READ_COILS:
        {
            ret = modbus_read_bits(context, start_address, num_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_DISCRETE_INPUTS:
        {
            ret = modbus_read_input_bits(context, start_address, num_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_HOLDING_REGISTERS:
        {
            ret = modbus_read_registers(context, start_address, num_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_INPUT_REGISTERS:
        {
            ret = modbus_read_input_registers(context, start_address, num_registers, ptr);
            break;
        }

        case MODBUS_FC_WRITE_SINGLE_COIL:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_SINGLE_REGISTER:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_MULTIPLE_COILS:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        {
            ret = -1;
            break;
        }

        default:
        {
            printf("Invalid MODBUS command [Hexadecimal 0x%x Integer %d]\n", read_type, read_type);
            ret = -1;
            break;
        }
    }

    if(ret == -1)
    {
        printf("ERROR reading register %s - start address %d type %d\n", modbus_strerror(errno), start_address, read_type);
    }
    else
    {
        printf("Read succes, printing %ld raw bytes\n", read_size);
        char *raw_data = ptr;

        for(int cx = 0; cx < read_size; cx++)
        {
            printf("%d ", raw_data[cx]);
        }

        printf("\n");

        switch (data_type)
        {
            case 'f':
            {
                float val = 0;

                uint16_t* sensordata = (uint16_t*)calloc(1, sizeof (uint16_t) * num_registers);
                memcpy(sensordata, ptr, read_size);

                printf("Scanner conversion to long and casting to float %0.2f\n", decode_long(sensordata[0], sensordata[1]) * calculation_factor);
                printf("Scanner default conversion to float %0.2f\n", decode_float(sensordata[0], sensordata[1]) * calculation_factor);
                printf("libmodbus default conversion to float %0.2f\n", modbus_get_float(&sensordata[0]) * calculation_factor);
                printf("libmodbus abcd conversion to float %0.2f\n", modbus_get_float_abcd(&sensordata[0]) * calculation_factor);
                printf("libmodbus bacd conversion to float %0.2f\n", modbus_get_float_badc(&sensordata[0]) * calculation_factor);
                printf("libmodbus cdab conversion to float %0.2f\n", modbus_get_float_cdab(&sensordata[0]) * calculation_factor);
                printf("libmodbus dcab conversion to float %0.2f\n", modbus_get_float_dcba(&sensordata[0]) * calculation_factor);

                if(num_registers < 2)
                {
                    long lval = sensordata[0];
                    val = lval * calculation_factor;
                    printf("Long to float %0.2f\n", val);
                }

                free(sensordata);

                break;
            }
            case 'l':
            {
                long val = 0;

                uint16_t sensordata[2] = {0};
                memcpy(&sensordata[0], ptr, read_size);
                printf("Bit shifting conversion %ld\n", val);

                break;
            }
            case 'c':
            {
                unsigned char* b = ptr;
                printf("Printing converted character value\n");
                printf("%c\n", b[1]);
                break;
            }
            default:
            {
                break;
            }
        }
    }

    if(ptr)
    {
        free(ptr);
    }

    modbus_close(context);
    modbus_free(context);

    return 0;
}

void print_help()
{
    printf("Usage\n");
    printf("<serial port> <baud rate> <parity> <data bits> <stop bits> <port timeout in seconds> <slave id> <read type> <start address> <number of registers> <data types> <calculation factor>\n");
}

long hex_to_decimal(char* hexVal)
{
    int len = strlen(hexVal);

    // Initializing base value to 1, i.e 16^0
    int base = 1;

    int dec_val = 0;

    // Extracting characters as digits from last character
    for (int i=len-1; i>=0; i--)
    {
        // if character lies in '0'-'9', converting
        // it to integral 0-9 by subtracting 48 from
        // ASCII value.
        if (hexVal[i]>='0' && hexVal[i]<='9')
        {
            dec_val += (hexVal[i] - 48)*base;

            // incrementing base by power
            base = base * 16;
        }

        // if character lies in 'A'-'F' , converting
        // it to integral 10 - 15 by subtracting 55
        // from ASCII value
        else if (hexVal[i]>='A' && hexVal[i]<='F')
        {
            dec_val += (hexVal[i] - 55)*base;

            // incrementing base by power
            base = base*16;
        }
    }

    return dec_val;
}

float decode_float(const unsigned int val0,const unsigned int val1)
{
  union endian pun;
  unsigned int i = 1;
  char *c = (char*)&i;

  if(*c)
  {
    pun.u = (val0 << 16) | val1;
  }
  else
  {
    pun.u = (val1 << 16) | val0;
  }

  return pun.f;
}

long decode_long(const unsigned int val0,const unsigned int val1)
{
    union endian pun;
    unsigned int i = 1;
    char *c = (char*)&i;

    if(*c)
    {
      pun.u = (val0 << 16) | val1;
    }
    else
    {
      pun.u = (val1 << 16) | val0;
    }

    return (float)pun.u;
}
