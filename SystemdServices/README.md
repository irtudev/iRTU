# Systemd Services

The systemd services will start the services and applications on iRTU boot.
The service files are to be placed in `/lib/systemd/system/` on the iRTU.

To activate or enable the service/application, so that it starts automatically on boot:
```
systemctl enable <service_file_name>
```

To deactivate or disable the service/application, so that it should not anymore automatically start on boot:
```
systemctl disable <service_file_name>
```

To start the service manually:
```
systemctl start <service_file_name>
```

To stop the service manually:
```
systemctl stop <service_file_name>
```

To restart the service manually:
```
systemctl restart <service_file_name>
```
