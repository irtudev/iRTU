cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(APP_VERSION_MAJOR 1)
set(APP_VERSION_MINOR 0)
set(APP_VERSION_PATCH 0)

set(Project AnanlogController)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore)

set(SOURCE
${SOURCE}
./src/dictionary.c
./src/iniparser.c
./src/AIOConfig.c
./src/AIOService.c
./src/AICalibration.c
./src/AIOService.c
./src/AnalogIn.c
./src/AnalogOut.c
./src/AOCalibration.c
./src/I2CInterface.c
./src/SPIInterface.c
./src/main.c
)

set(HEADERS
${HEADERS}
./include/dictionary.h
./include/iniparser.h
./include/AIOConfig.h
./include/AIOService.h
./include/AICalibration.h
./include/AnalogIn.h
./include/AnalogOut.h
./include/AOCalibration.h
./include/I2CInterface.h
./include/SPIInterface.h
./include/AIOService.h
)

set(CONFIG
${CONFIG}
./etc/controller_analog.conf
)

add_executable(controller_analog ${SOURCE} ${HEADERS})

install(TARGETS controller_analog DESTINATION /usr/bin)
install(FILES ${CONFIG} DESTINATION /etc)
set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_PATCH})

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Subrato Roy")
set(CPACK_PACKAGE_DESCRIPTION, "Analog IO Controller")

include(CPack)


