#include "AIOService.h"
#include "AnalogIn.h"
#include "AnalogOut.h"
#include "AIOConfig.h"
#include "AICalibration.h"
#include "AOCalibration.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>


static void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);

static configuration_t* config = NULL;
static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;
static char** destination_list = NULL;
static long sampling_rate = 10;

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    const char* destinations = NULL;
    destinations = configuration_get_value_as_string(config, "default", "destination");
    destination_list = strsplitchar(destinations, ',');
    sampling_rate = configuration_get_value_as_integer(config, "default", "sampling_rate");

    if(sampling_rate)
    {
        sampling_rate = 600;
    }

    configuration_release(config);

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);
    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    int ctr = 0;

    bool continue_loop = true;

    while(continue_loop)
    {
        sleep((unsigned int)sampling_rate);

        char* payload = NULL;

        for(int tChNum = CHANNEL_NUM_1 ; tChNum < global_analog_in_configuration.mNumOfCnannel ; ++tChNum )
        {
            float tEnggData = 0;
            unsigned short int tBinData = 0;

            tBinData = Adc_ReadData((ChannelNum_e)tChNum);
            Adc_ConvertBin2Engg((ChannelNum_e)tChNum, tBinData, &tEnggData);

            printf("CH%02d : 0x%04X, %3.2f\n", tChNum+1, tBinData, tEnggData);

            sleep(1);
        }

        for(int tChNum = 0 ; tChNum < global_analog_out_configuration.mNumOfChannel ; ++tChNum)
        {
            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0xFFF);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 10000));

            sleep(5);

            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0x7FF);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 5000));

            sleep(5);

            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0x000);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 0));
        }

        for(int index = 0; destination_list[index] != NULL; index++)
        {
            if(!message_bus_send(message_bus, destination_list[index], Data, UserData, Text, payload, strlen(payload)))
            {
                continue_loop = false;
                break;
            }
        }

        ctr++;
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    strfreelist(destination_list);
    logger_release(logger);

    return false;
}

void service_calibrate_ADC()
{

}

void service_calibrate_DAC()
{

}

void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    if(ptype == Data || mtype == LoopBack)
    {
        WriteInformation(logger, "Loopback successfull");
    }

    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char error_message[64];

        char** command_lines = strsplitchar(messagebuffer, '\n');

        char* command_name = command_lines[0];
        char* command_string = command_lines[1];

        char** command_values = strsplitchar(command_string, ',');

        for(int index = 0; command_values[index] != NULL; index++)
        {
            char** calibration_args = strsplitchar(command_values[index], ':');

            // String is composed as PIN:SIGNAL
            int channel = atoi(calibration_args[0]);
            int enabled = atoi(calibration_args[1]);
            int aiotype = atoi(calibration_args[2]);
            int low = atoi(calibration_args[3]);
            int mid = atoi(calibration_args[4]);
            int high = atoi(calibration_args[5]);

            memset(error_message, 0, sizeof (error_message));

            if(strcmp(command_name, "CALIBRATE_DAC") == 0)
            {
                Dac_calibration();
            }
            else
            {
                if(strcmp(command_name, "CALIBRATE_ADC") == 0)
                {
                    Adc_calibration();
                }
            }

            strfreelist(calibration_args);
        }

        strfreelist(command_lines);
        strfreelist(command_values);
    }

}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
