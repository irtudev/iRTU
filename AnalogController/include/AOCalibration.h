//
// Created by mukeshp on 10-Aug-20.
//

#ifndef AIO_PERIPHERAL_SERVICE_AOCALIBRATION_H
#define AIO_PERIPHERAL_SERVICE_AOCALIBRATION_H

typedef struct AoCalibration
{
    unsigned short int mAoLowCal;
    unsigned short int mAoMidCal;
    unsigned short int mAoHighCal;
}AoCalibration_t;

void AoCalibrationInit(struct AoCalibration* ptr);

int Dac_calibration(void);
int Dac_systemLowCalibration(int iChannelNum, unsigned short int iData);
int Dac_systemMidCalibration(int iChannelNum, unsigned short int iData);
int Dac_systemHighCalibration(int iChannelNum, unsigned short int iData);
int GetDacCount(int iChannelNum, int iVolInmV);

#endif //AIO_PERIPHERAL_SERVICE_AOCALIBRATION_H
