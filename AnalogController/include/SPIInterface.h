//
// Created by mukeshp on 5/20/2020.
//

#ifndef DEVELOPEMENT_SPI_INTERFACE_H
#define DEVELOPEMENT_SPI_INTERFACE_H

#include <linux/spi/spidev.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>

int SpiOpen(const char *pDeviceName);
int SpiOpen_Hart(const char *pDeviceName);
//int SpiWrite(int iFd, unsigned char *pTx, unsigned long iDataLen);
int SpiWrite(int iFd, unsigned char *pTx, unsigned long iDataLen, char iCsAfterWr);
//int SpiRead(int iFd, unsigned char *pTx, unsigned char *pRx, unsigned long iDataLen);
int SpiRead(int iFd, unsigned char *pTx, unsigned long iTxDataLen, unsigned char *pRx, unsigned long iRxDataLen);
int SpiDataTransfer(int iFd, unsigned char *pRx, unsigned long iDataLen);
void SpiClose(int iFd);
int SpiDataTransfer_hart(int iFd, unsigned char *pTx, unsigned char *pRx, unsigned long iDataLen);
int SpiRead_Hart(int iFd, unsigned char *pTx, unsigned char *pRx, unsigned long iDataLen);
int SpiWrite_Hart(int iFd, unsigned char *pTx, unsigned long iDataLen, char iCsAfterWr);

#endif //DEVELOPEMENT_SPI_INTERFACE_H
