#ifndef ANALOG_IO_SERVICE
#define ANALOG_IO_SERVICE

#include <stdbool.h>

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();
void service_calibrate_ADC();
void service_calibrate_DAC();

#endif
