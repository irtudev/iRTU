//
// Created by mukeshp on 31-Jul-20.
//

#ifndef AIINTERFACE_AICALIBRATION_H
#define AIINTERFACE_AICALIBRATION_H

#include "AnalogIn.h"

typedef struct AiCalibration
{
    unsigned short int mAiLowCal;
    unsigned short int mAiMidCal;
    unsigned short int mAiHighCal;
}AiCalibration_t;

void AiCalibrationInit(struct AiCalibration* ptr);

int Adc_systemLowCalibration(ChannelNum_e iChannelNum, unsigned short int iData);
int Adc_systemMidCalibration(ChannelNum_e iChannelNum, unsigned short int iData);
int Adc_systemHighCalibration(ChannelNum_e iChannelNum, unsigned short int iData);
void Adc_ConvertBin2Engg(ChannelNum_e iChannelNum, unsigned short int tAnalogDataBin, float *pAnalogDataEngg);
int Adc_calibration(void);

#endif //AIINTERFACE_AICALIBRATION_H
