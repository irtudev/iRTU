#ifndef VIBRATION_PROCESSOR
#define VIBRATION_PROCESSOR

#include <stdbool.h>

bool app_initialize();
bool app_destroy();
bool app_start();
bool app_restart();
bool app_stop();

#endif
