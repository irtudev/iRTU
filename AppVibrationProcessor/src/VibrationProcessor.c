#include "VibrationProcessor.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/StringList.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Dictionary.h>

static void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);

static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;
static configuration_t* config = NULL;
static string_list_t* destination_list = NULL;
static dictionary_t* domain_payloads = NULL;
static char** domains = NULL;

bool app_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    char* value = NULL;
    value = (char*)configuration_get_value_as_string(config, "default", "destination");
    destination_list = str_list_allocate_from_string(destination_list, value, ",");

    domains = configuration_get_all_keys(config, "payloads");

    if(domains != NULL)
    {
        domain_payloads = dictionary_allocate();

        for(int index = 0; domains[index] != NULL; index++)
        {
            value = (char*)configuration_get_value_as_string(config, "payloads", domains[index]);
            dictionary_set_value(domain_payloads, domains[index], strlen(domains[index]), value, strlen(value));
        }
    }

    configuration_release(config);

    return true;
}

bool app_destroy()
{
    return true;
}

bool app_start()
{
    message_bus = message_bus_initialize(on_network_event);

    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    bool continue_loop = true;

    while(continue_loop)
    {
        sleep(10);

        char* destination = str_list_get_first(destination_list);

        while(destination)
        {           
            for(int index = 0; domains[index] != NULL; index++)
            {
                char* payload = dictionary_get_value(domain_payloads, domains[index], strlen(domains[index]));
                if(!message_bus_send(message_bus, destination, Data, UserData, Text, payload, strlen(payload)))
                {
                    continue_loop = false;
                    break;
                }
            }

            destination = str_list_get_next(destination_list);
        }
    }

    return true;
}

bool app_restart()
{
    return false;
}

bool app_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    str_list_free(destination_list);
    dictionary_free(domain_payloads);
    logger_release(logger);
    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{  
    char str[64] = {0};

    if(mtype == Register)
    {
        sprintf(str, "%s is online\n", messagebuffer);
        WriteInformation(logger, str);
        return;
    }

    if(mtype == DeRegister)
    {
        sprintf(str, "%s is offline\n", messagebuffer);
        WriteInformation(logger, str);
        return;
    }

    if(mtype == LoopBack)
    {
        WriteInformation(logger,"Loopback test successfull");
        return;
    }

    WriteInformation(logger,messagebuffer);
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            app_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
