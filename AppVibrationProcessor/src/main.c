#include "VibrationProcessor.h"

int main(int argc, char* argv[])
{
    if(!app_initialize())
    {
        return -1;
    }

    app_start();

    app_stop();

    return 0;
}
