#ifndef PROTOCOL_MODBUS
#define PROTOCOL_MODBUS

#include "IProtocolRS485.h"
#include <stdbool.h>
#include <RTUCore/Logger.h>

typedef struct SerialPort
{
    char serial_device_name[48];
    int baud_rate;
    char parity;
    int data_bits;
    int stop_bits;
    unsigned int timeout_seconds;
    char xon_off;
}SerialPort;

typedef struct ModbusQuery
{
    float adjustment_factor;
    unsigned char query_type;
    unsigned int start_address;
    unsigned int num_of_registers;
    char property_name[33];
    char property_value[13];
    char conversion_type;
    char converter;
}ModbusQuery;

typedef struct Modbus
{
    unsigned char slave_id;
    SerialPort serial_port;
    unsigned int num_of_query;
    ModbusQuery* modbus_query_configuration;
}Modbus;

bool proto_modbus_initialize(logger_t* logger, Modbus* ptr);
bool proto_modbus_destroy(logger_t* logger);
void* proto_modbus_get_configuration(logger_t* logger);
int proto_modbus_get_query_count(logger_t* logger);
bool proto_modbus_read_all(logger_t* logger);
void* proto_modbus_read_index(logger_t* logger, unsigned int index);
void* proto_modbus_read_query(logger_t* logger, ModbusQuery* query);
bool proto_modbus_write(logger_t* logger, unsigned int index, void* buffer);

#endif
