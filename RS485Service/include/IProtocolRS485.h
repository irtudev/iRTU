#ifndef I_PROTOCOL_RS485
#define I_PROTOCOL_RS485

typedef struct Modbus Modbus;
typedef struct Canbus Canbus;
typedef struct DNP3 DNP3;

typedef struct IProtocolRS485
{
    union
    {
        Modbus *pModbus;
        Canbus *pCanbus;
        DNP3 *pDNP3;
    };

}IProtocolRS485;

#endif
