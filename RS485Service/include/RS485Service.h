#ifndef RS485_SERVICE
#define RS485_SERVICE

#include <stdbool.h>

#if !defined(bool)
#define bool int
#define false 0
#define true 1
#endif

#if !defined(NULL)
#define NULL 0
#endif

bool service_initialize(const char* config_file);
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
