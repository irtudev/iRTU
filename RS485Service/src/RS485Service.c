#include "RS485Service.h"
#include "ProtocolMODBUS.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/Directory.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Buffer.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);
static bool read_modbus_configuration(void);
static void read_send_all(const char* node_name);
static void read_send(const char *node_name, int query_index);

static Modbus* params_modbus = NULL;
static configuration_t* config = NULL;
static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;

static  char** destination_list;
static bool enabled = false;
static char peripheral_id[21] = {0};
static char protocol_name[13] = {0};
static long sampling_rate = 600;
static char peripheral_name[65] = {0};
static bool debug = false;

bool service_initialize(const char* config_file)
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    if(config_file == NULL)
    {
        config = configuration_allocate_default();
    }
    else
    {
        config = configuration_allocate(config_file);
    }

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    const char* value = NULL;
    value = configuration_get_value_as_string(config, "default", "destination");

    char *temp = (char*)calloc(1, strlen(value)+10);
    strcpy(temp, value);

    if(strindexofchar(value, ',') < 0)
    {
        strcat(temp, ", null");
    }

    destination_list = strsplitchar(temp, ',');
    free(temp);

    strcpy(protocol_name, configuration_get_value_as_string(config, "default", "protocol"));
    enabled = configuration_get_value_as_boolean(config, "default", "enabled");
    sampling_rate = configuration_get_value_as_integer(config, "default", "sampling_rate");
    debug = configuration_get_value_as_boolean(config, "default", "debug");

    if(sampling_rate == 0)
    {
        sampling_rate = 600;
    }

    if(strcmp(protocol_name, "modbus") == 0)
    {
        if(!read_modbus_configuration())
        {
            service_stop();
            return false;
        }
    }

    configuration_release(config);
    config = NULL;

    return true;
}

bool read_modbus_configuration()
{
    strncpy(peripheral_id, configuration_get_value_as_string(config, "modbus", "peripheral_id"), 20);
    strncpy(peripheral_name, configuration_get_value_as_string(config, "modbus", "peripheral_name"), 64);
    params_modbus = (Modbus*)calloc(1, sizeof (Modbus));

    strcpy(params_modbus->serial_port.serial_device_name, configuration_get_value_as_string(config, "serial_port", "device"));
    params_modbus->serial_port.baud_rate = (int)configuration_get_value_as_integer(config, "serial_port", "baud_rate");
    params_modbus->serial_port.data_bits = (char)configuration_get_value_as_integer(config, "serial_port", "data_bits");
    params_modbus->serial_port.stop_bits = (char)configuration_get_value_as_integer(config, "serial_port", "stop_bits");
    params_modbus->serial_port.xon_off = (char)configuration_get_value_as_integer(config, "serial_port", "xon_off");
    params_modbus->serial_port.timeout_seconds = (unsigned int)configuration_get_value_as_integer(config, "serial_port", "port_timeout");
    params_modbus->serial_port.parity = configuration_get_value_as_string(config, "serial_port", "parity")[0];

    params_modbus->slave_id = (unsigned char)configuration_get_value_as_integer(config, "modbus", "slave_id");
    params_modbus->num_of_query = (unsigned int)configuration_get_value_as_integer(config, "modbus", "query_count");

    params_modbus->modbus_query_configuration = (ModbusQuery*)calloc(params_modbus->num_of_query, sizeof(ModbusQuery));
    long index = 0;
    for(index = 0; index < params_modbus->num_of_query; index++)
    {
        char section[16] = {0};
        sprintf(section, "query%ld", index+1);
        if(configuration_has_section(config, section))
        {
            WriteInformation(logger, section);
            params_modbus->modbus_query_configuration[index].start_address = (int)configuration_get_value_as_integer(config, section, "start_address");
            params_modbus->modbus_query_configuration[index].query_type = (int)configuration_get_value_as_integer(config, section, "input_type");
            params_modbus->modbus_query_configuration[index].num_of_registers  = (int)configuration_get_value_as_integer(config, section, "num_of_registers");
            params_modbus->modbus_query_configuration[index].conversion_type = configuration_get_value_as_char(config, section, "data_type");
            params_modbus->modbus_query_configuration[index].adjustment_factor = (float)configuration_get_value_as_real(config, section, "adjustment_factor");
            strcpy(params_modbus->modbus_query_configuration[index].property_name, configuration_get_value_as_string(config, section, "property_name"));
            params_modbus->modbus_query_configuration[index].converter =  configuration_get_value_as_string(config, section, "converter")[0];
            WriteInformation(logger, params_modbus->modbus_query_configuration[index].property_name);
        }
    }

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    if(enabled)
    {
        if(!proto_modbus_initialize(logger, params_modbus))
        {
            free(params_modbus);
            params_modbus = NULL;
            WriteLog(logger, protocol_name, LOG_ERROR);
            WriteLog(logger, "Could not initialize port or no device found", LOG_ERROR);
            return false;
        }
    }

    message_bus = message_bus_initialize(on_network_event);
    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    while(true)
    {
        for(int ctr = 0; destination_list[ctr] != NULL; ctr++)
        {
            read_send_all(destination_list[ctr]);

            if(debug)
            {
                return true;
            }
        }

        usleep((unsigned int)sampling_rate);
    }

    return true;
}

void read_send_all(const char* node_name)
{
    buffer_t* payload =  buffer_allocate_default();

    proto_modbus_read_all(logger);

    buffer_append_char(payload, '"');
    buffer_append_string(payload, "peripheral_id");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, peripheral_id);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "peripheral");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, peripheral_name);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');
    buffer_append_string(payload, "\"timestamp\":");
    buffer_append_curr_timestamp(payload);
    buffer_append_string(payload, ",");

    for(int index = 0; index < params_modbus->num_of_query; index++)
    {
        buffer_append_char(payload, '"');
        buffer_append_string(payload, params_modbus->modbus_query_configuration[index].property_name);
        buffer_append_char(payload, '"');
        buffer_append_char(payload, ':');
        buffer_append_string(payload, params_modbus->modbus_query_configuration[index].property_value);
        buffer_append_char(payload, ',');
    }

    buffer_remove_end(payload, 1);

    if(message_bus_send(message_bus, node_name, Data, Text, (char*)buffer_get_data(payload), (long)buffer_get_size(payload)))
    {
        if(debug)
        {
            WriteInformation(logger, (char*)buffer_get_data(payload));
        }
    }
    else
    {
        WriteLog(logger, "Send failure over IPC", LOG_ERROR);
    }

    buffer_free(payload);
}

void read_send(const char *node_name, int query_index)
{
    buffer_t* payload =  buffer_allocate_default();

    proto_modbus_read_index(logger, query_index);

    buffer_append_char(payload, '"');
    buffer_append_string(payload, "peripheral_id");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, peripheral_id);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "peripheral");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, peripheral_name);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');
    buffer_append_string(payload, "\"timestamp\":");
    buffer_append_curr_timestamp(payload);
    buffer_append_string(payload, ",");

    buffer_append_char(payload, '"');
    buffer_append_string(payload, params_modbus->modbus_query_configuration[query_index].property_name);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_string(payload, params_modbus->modbus_query_configuration[query_index].property_value);

    if(!message_bus_send(message_bus, node_name, Data, Text, (char*)buffer_get_data(payload), (long)buffer_get_size(payload)))
    {
        if(debug)
        {
            WriteInformation(logger, (char*)buffer_get_data(payload));
        }
    }

    buffer_free(payload);
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    if(message_bus)
    {
        message_bus_close(message_bus);
        message_bus_release(message_bus);
    }

    if(destination_list)
    {
        strfreelist(destination_list);
    }

    if(logger)
    {
        logger_release(logger);
    }

    if(params_modbus)
    {
        proto_modbus_destroy(logger);
        free(params_modbus);
    }

    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char** command_lines = strsplitchar(messagebuffer, '\n');

        char* command_name = command_lines[0];
        char* command_string = command_lines[1];

        if(strstr(command_name, "WRITE"))
        {
            char** command_values = strsplitchar(command_string, ',');;

            for(int index = 0; command_values[index] != NULL; index++)
            {
                // String is composed as QUERY INDEX: DATA
                int qeury_index = command_values[index][0];
                void* data_ptr = &command_values[index][2];
                proto_modbus_write(logger, qeury_index, data_ptr);
            }

            strfreelist(command_values);
        }

        if(strstr(command_name, "READ"))
        {
            if(!command_string)
            {
                read_send_all(node_name);
            }
            else
            {
                char** command_values = strsplitchar(command_string, ',');;

                for(int index = 0; command_values[index] != NULL; index++)
                {
                    // String is composed as Query index only
                    int qeury_index = command_values[index][0];
                    read_send(node_name, qeury_index);
                }

                strfreelist(command_values);
            }
        }

        strfreelist(command_lines);
    }

}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW RESIZED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
