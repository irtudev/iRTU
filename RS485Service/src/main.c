#include "RS485Service.h"
#include <string.h>

int main(int argc, char* argv[])
{
    if(argc == 3)
    {
        if(strcmp(argv[1], "-c") == 0)
        {
            if(!service_initialize(argv[2]))
            {
                return -1;
            }
        }
    }
    else
    {
        if(!service_initialize(NULL))
        {
            return -1;
        }
    }

    service_start();

    service_stop();

    return 0;
}
