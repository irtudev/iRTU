#include "ProtocolMODBUS.h"
#include <string.h>
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <modbus/modbus.h>

#define MAX_MODBUS_QUERY 65535

static Modbus* modbus = NULL;
static modbus_t *context = NULL;

long convert_hexadecimal_to_decimal(const char* str);
float decode_float(const unsigned int val0,const unsigned int val1);
long decode_long(const unsigned int val0,const unsigned int val1);

union endian
{
  float f;
  unsigned int u;
};

bool proto_modbus_initialize(logger_t* logger, Modbus* ptr)
{
    if(!ptr)
    {
        return false;
    }

    context = modbus_new_rtu(ptr->serial_port.serial_device_name, ptr->serial_port.baud_rate, ptr->serial_port.parity, ptr->serial_port.data_bits, ptr->serial_port.stop_bits);

    if (NULL == context)
    {
        WriteLog(logger, "Could not get a MODBUS context", LOG_ERROR);
        return false;
    }

    modbus_set_slave(context, ptr->slave_id);
    modbus_set_response_timeout(context, ptr->serial_port.timeout_seconds, 0);

    if (-1 == modbus_connect(context))
    {
        WriteLog(logger, "Could not connect to the MODBUS device", LOG_ERROR);
        modbus_free(context);
        return false;
    }

    modbus = ptr;
    WriteLog(logger, "Saved MODBUS context", LOG_INFO);
    return true;
}

bool proto_modbus_destroy(logger_t* logger)
{
    if(!modbus)
    {
        return false;
    }

    modbus_close(context);
    modbus_free(context);

    if(modbus->modbus_query_configuration)
    {
        free(modbus->modbus_query_configuration);
    }

    return true;
}

void* proto_modbus_get_configuration(logger_t *logger)
{   
    return modbus;
}

int proto_modbus_get_query_count(logger_t *logger)
{
    if(!modbus)
    {
        return 0;
    }

    return modbus->num_of_query;
}

bool proto_modbus_read_all(logger_t* logger)
{
    if(!modbus || !logger)
    {
        WriteLog(logger, "NULL modbus object passed, returning", LOG_ERROR);
        return false;
    }

    for(int index = 0; index < modbus->num_of_query; index++)
    {
        memset(modbus->modbus_query_configuration[index].property_value, 0, 13);
    }

    for(int index = 0; index < modbus->num_of_query; index++)
    {
        ModbusQuery* query = &modbus->modbus_query_configuration[index];
        proto_modbus_read_query(logger, query);
    }

    return true;
 }

void *proto_modbus_read_index(logger_t *logger, unsigned int index)
{
    if(!modbus || !logger)
    {
        return NULL;
    }

    if((int)index < 0 || index > modbus->num_of_query - 1)
    {
        return NULL;
    }

    ModbusQuery* query = &modbus->modbus_query_configuration[index];

    return  proto_modbus_read_query(logger, query);
}

void* proto_modbus_read_query(logger_t *logger, ModbusQuery* query)
{
    if(!modbus || !query)
    {
        return NULL;
    }

    void* ptr = NULL;
    int ret = 0;
    unsigned long read_size;

    memset(query->property_value, 0, 13);

    if(query->query_type == MODBUS_FC_READ_COILS || query->query_type == MODBUS_FC_READ_DISCRETE_INPUTS)
    {
        read_size = sizeof(uint8_t)* (unsigned long)query->num_of_registers;
    }
    else
    {
        read_size = sizeof(uint16_t)* (unsigned long)query->num_of_registers;
    }

    ptr = calloc(1, read_size);

    switch(query->query_type)
    {
        case MODBUS_FC_READ_COILS:
        {
            ret = modbus_read_bits(context, query->start_address, query->num_of_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_DISCRETE_INPUTS:
        {
            ret = modbus_read_input_bits(context, query->start_address, query->num_of_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_HOLDING_REGISTERS:
        {
            ret = modbus_read_registers(context, query->start_address, query->num_of_registers, ptr);
            break;
        }

        case MODBUS_FC_READ_INPUT_REGISTERS:
        {
            ret = modbus_read_input_registers(context, query->start_address, query->num_of_registers, ptr);
            break;
        }

        case MODBUS_FC_WRITE_SINGLE_COIL:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_SINGLE_REGISTER:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_MULTIPLE_COILS:
        {
            ret = -1;
            break;
        }

        case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        {
            ret = -1;
            break;
        }

        default:
        {
            printf("Invalid MODBUS command [Hexadecimal 0x%x Integer %d]\n", query->query_type, query->query_type);
            ret = -1;
            break;
        }
    }

    if(ret == -1)
    {
        char errmsg[128] = {0};
        snprintf(errmsg, 127, "ERROR reading register %s - start address 0x%x type %d\n", modbus_strerror(errno), query->start_address, query->query_type);
        strcpy(query->property_value, "0");
        WriteLog(logger, errmsg, LOG_ERROR);

        if(ptr)
        {
            free(ptr);
        }

        return NULL;
    }

    switch (query->conversion_type)
    {
        case 'f':
        {
            float val = 0;

            uint16_t sensordata[2] = {0};
            memcpy(&sensordata[0], ptr, read_size);

            switch (query->converter)
            {
                case 'a':
                {
                    val = modbus_get_float_abcd(&sensordata[0]) * query->adjustment_factor;
                    break;
                }
                case 'b':
                {
                    val = modbus_get_float_badc(&sensordata[0]) * query->adjustment_factor;
                    break;
                }
                case 'c':
                {
                    val = modbus_get_float_cdab(&sensordata[0]) * query->adjustment_factor;
                    break;
                }
                case 'd':
                {
                    val = modbus_get_float_dcba(&sensordata[0]) * query->adjustment_factor;
                    break;
                }
                case 'r':
                {
                    long lval = sensordata[0];
                    val = lval * query->adjustment_factor;
                    break;
                }
                case 'l':
                {
                    val = decode_long(sensordata[0], sensordata[1]) * query->adjustment_factor;
                    break;
                }
                case 'f':
                {
                    val = decode_float(sensordata[0], sensordata[1]) * query->adjustment_factor;
                    break;
                }
                case 'h':
                {
                    break;
                }
                case 'm':
                default:
                {
                    val = modbus_get_float(&sensordata[0]) * query->adjustment_factor;
                    break;
                }
            }

            sprintf(query->property_value, "%0.2f", val);

            break;
        }
        case 'l':
        {
            long val = 0;

            uint16_t sensordata[2] = {0};
            memcpy(&sensordata[0], ptr, read_size);

            switch (query->converter)
            {
                case 'l':
                {
                    val = decode_long(sensordata[0], sensordata[1]);
                    break;
                }
                case 'h':
                {
                    char str[32] = {0};
                    sprintf(str, "0x%x", sensordata[0]);
                    val = convert_hexadecimal_to_decimal(str);
                    break;
                }
            }

            sprintf(query->property_value, "%ld", val);
            break;
        }
        case 'c':
        {
            unsigned char* b = ptr;
            sprintf(query->property_value, "%c", b[1]);
            break;
        }
        default:
        {
            break;
        }
    }

    if(ptr)
    {
        free(ptr);
    }

    return &query->property_value;
}

bool proto_modbus_write(logger_t* logger, unsigned int index, void* buffer)
{
    return false;
}

long convert_hexadecimal_to_decimal(const char* hexVal)
{
    int len = strlen(hexVal);

    // Initializing base value to 1, i.e 16^0
    int base = 1;

    int dec_val = 0;

    // Extracting characters as digits from last character
    for (int i=len-1; i>=0; i--)
    {
        // if character lies in '0'-'9', converting
        // it to integral 0-9 by subtracting 48 from
        // ASCII value.
        if (hexVal[i]>='0' && hexVal[i]<='9')
        {
            dec_val += (hexVal[i] - 48)*base;

            // incrementing base by power
            base = base * 16;
        }

        // if character lies in 'A'-'F' , converting
        // it to integral 10 - 15 by subtracting 55
        // from ASCII value
        else if (hexVal[i]>='A' && hexVal[i]<='F')
        {
            dec_val += (hexVal[i] - 55)*base;

            // incrementing base by power
            base = base*16;
        }
    }

    return dec_val;}

float decode_float(const unsigned int val0,const unsigned int val1)
{
  union endian pun;
  unsigned int i = 1;
  char *c = (char*)&i;

  if(*c)
  {
    pun.u = (val0 << 16) | val1;
  }
  else
  {
    pun.u = (val1 << 16) | val0;
  }

  return pun.f;
}

long decode_long(const unsigned int val0,const unsigned int val1)
{
    union endian pun;
    unsigned int i = 1;
    char *c = (char*)&i;

    if(*c)
    {
      pun.u = (val0 << 16) | val1;
    }
    else
    {
      pun.u = (val1 << 16) | val0;
    }

    return pun.u;
}
