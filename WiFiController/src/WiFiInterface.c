//
// Created by mukeshp on 14-Aug-20.
//
#include "WiFiInterface.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <errno.h>

wifiSetting_t gWifiSetting;

void wifi_init_defaults(struct wifiSetting* ptr)
{
    ptr->mEnable = 0;
    strcpy(ptr->mSsid,"Cimcon 6th Floor Conf.");
    strcpy(ptr->mPassword, "csipl@123");
}

void wifi_start_controller(void)
{
    int tInterfaceStatus = 0;

    wifi_setting();
    wifi_connect();

    while (1)
    {
        sleep(10);
        if(Is_Interface_Present((char *)"wlan0"))
        {
            tInterfaceStatus = Get_Interface_Status("wlan0");
            if ((tInterfaceStatus & IFF_UP) && (tInterfaceStatus & IFF_RUNNING))
            {
                printf("wlan0 interface is up and running\n");
            }
            else
            {
                printf("wlan0 interface is not present\n");
                wifi_disconnect();
                sleep(5);
                wifi_connect();
            }
        }
    }
}

int wifi_setting(void)
{
    char tString[512] = {0, };

    sprintf(tString, "echo \"wpa-ssid %s\nwpa-psk %s\" > /etc/network/interfaces.d/wlan0",
                            gWifiSetting.mSsid, gWifiSetting.mPassword);
    system(tString);

    return 0;
}


int wifi_connect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "nmcli device connect wlan0");
    system(tCommand);

    return 0;
}

int wifi_disconnect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "nmcli device disconnect wlan0");
    system(tCommand);

    return 0;
}

int Is_Interface_Present(char * ifname)
{
    struct ifaddrs *ifaddr, *ifa;
    int n=0;

    if(getifaddrs(&ifaddr) == -1)
    {
        printf("Error in getifaddrs at %s (%s)\n",__func__, strerror(errno));
        return -1;
    }

    for(ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
    {
        if(strstr(ifa->ifa_name,ifname) == NULL)
        {
            //required interface found
            freeifaddrs(ifaddr);
            return 1;
        }
    }

    freeifaddrs(ifaddr);
    return 0;
}

int Get_Interface_Status(const char * ifname)
{
    int fd;
    struct ifreq ifr;

    /* AF_INET - to define network interface IPv4*/
    /* Creating soket for it.*/
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        printf("Get_Interface_Status error (%s) at %s\n", strerror(errno), __func__ );
        return 0;
    }

    /*AF_INET - to define IPv4 Address type.*/
    ifr.ifr_addr.sa_family = AF_INET;

    /*eth0 - define the ifr_name - port name where network attached.*/
    memcpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

    /*Accessing network interface information by passing address using ioctl.*/
    if(ioctl(fd, SIOCGIFFLAGS, &ifr) < 0)
    {
        printf("ioctl error (%s) at %s\n", strerror(errno), __func__ );
        close(fd);
        return 0;
    }

    /*closing fd*/
    close(fd);
    if(ifr.ifr_flags)
    {
        return ifr.ifr_flags;
    }

    return 0;
}
