#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "WiFiInterface.h"
#include <RTUCore/Configuration.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Logger.h>

static logger_t* logger = NULL;
static configuration_t* config = NULL;
static void on_signal_received(SignalType stype);

extern wifiSetting_t gWifiSetting;

void SignalTrap(int signum) __attribute__ ((noreturn));

int main(int argc, char**argv)
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    wifi_init_defaults(&gWifiSetting);

    gWifiSetting.mEnable = configuration_get_value_as_char(config, "wifi", "enable");
    strncpy(gWifiSetting.mSsid, configuration_get_value_as_string(config, "wifi", "ssid"), sizeof(gWifiSetting.mSsid)-1);
    strncpy(gWifiSetting.mPassword, configuration_get_value_as_string(config, "wifi", "password"), sizeof(gWifiSetting.mPassword)-1);

    configuration_release(config);

    if(gWifiSetting.mEnable == '1')
    {
       wifi_start_controller();
    }
    else
    {
        wifi_disconnect();
    }

    while (1) {sleep(30);}
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}

