//
// Created by mukeshp on 14-Aug-20.
//

#ifndef WIFI_CONTROLLER
#define WIFI_CONTROLLER

#include <string.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct wifiSetting
{
    int mEnable;
    char mSsid[256];
    char mPassword[256];
}wifiSetting_t;

void wifi_init_defaults(struct wifiSetting* ptr);

void wifi_start_controller(void) __attribute__ ((noreturn));
int wifi_setting(void);
int wifi_connect(void);
int wifi_disconnect(void);

int Is_Interface_Present(char * ifname);
int Get_Interface_Status(const char * ifname);

extern wifiSetting_t gWifiSetting;

#endif //IOSERVICE_WIFIINTERFACE_H
