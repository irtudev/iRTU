#ifndef ANALOG_IO_SERVICE
#define ANALOG_IO_SERVICE

#include <stdbool.h>
#include <RTUCore/Logger.h>

#define AI_CHANNEL_ENABLE          (1)
#define AI_CHANNEL_DISABLE         (0)

#define AI_CHANNEL_TYPE_BIPOLAR_10V     'V'
#define AI_CHANNEL_TYPE_4_20mA          'I'

#define AI_CHANNEL_MAX (4)

#define GET_AI_CHANNEL_TYPE_STR(_arg_)  (((_arg_) == AI_CHANNEL_TYPE_BIPOLAR_10V) ? "Voltage" : "Current")

typedef struct Channel
{
    bool enabled;
    char channel_type;
    long sys_low_cal;
    long sys_mid_cal;
    long sys_high_cal;
    char name[33];
}Channel;

typedef struct AIOConfiguration
{
    long channel_in_count;
    long channel_out_count;
    Channel* channels_in;
    Channel* channels_out;
}AIOConfiguration;

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

extern AIOConfiguration analogIOConfiguration;

#endif
