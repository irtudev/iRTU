//
// Created by mukeshp on 5/19/2020.
//

#ifndef DEVELOPEMENT_ANALOGOUT_H
#define DEVELOPEMENT_ANALOGOUT_H

#include <RTUCore/Logger.h>

//

#define AO_CHANNEL_MAX  (2)

/* @Pending: Device Name search is pending, consolidated function creation pending, testing pending. */
#define AO1_I2C_SLAVE_ADDR (0x90)	/* The AO1 - I2C slave address */
#define AO2_I2C_SLAVE_ADDR (0x92)	/* The AO2 - I2C slave address */

#define GET_AO_CHANNEL_ADDR(_arg_)  (((_arg_) == 0) ? (AO1_I2C_SLAVE_ADDR) : (AO2_I2C_SLAVE_ADDR))

#define AO_CHANNEL_NUM_1        (0)
#define AO_CHANNEL_NUM_2        (1)

#define AO_CHANNEL_ENABLE          (1)
#define AO_CHANNEL_DISABLE         (0)

int DacInit(logger_t* logger);
int Dac_write(logger_t* logger, char iDevice, unsigned short int iData);

int DacReadDeviceIdentification(logger_t* logger, char iSlaveAddr);
int DacSetOperationMode(logger_t* logger, char iSlaveAddr, unsigned short int iMode);
int DacSoftReset(logger_t* logger, char iSlaveAddr);
int DacLoadSynchronously(logger_t* logger, char iSlaveAddr);
int DcaSelectReferenceVoltage(logger_t* logger, char iSlaveAddr, char iRef);
int DacSetGainAndDivisor(logger_t* logger, char iSlaveAddr, unsigned char iMul, unsigned char iDiv);
int DacWriteData(logger_t* logger, char iSlaveAddr, unsigned short int iData);
int DacReadData(logger_t* logger, char iSlaveAddr, unsigned short int *pData);
int DacReadStatusRegister(logger_t* logger, char iSlaveAddr, unsigned short int *pData);
int DacGetGainAndDivisor(logger_t* logger, char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv);
int DacWriteTriggerRegister(logger_t* logger, char iSlaveAddr, unsigned short int iData);

#endif //DEVELOPEMENT_ANALOGOUT_H
