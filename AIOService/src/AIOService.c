#include "AIOService.h"
#include "AnalogIn.h"
#include "AnalogOut.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Buffer.h>

static void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);
static void read_analog_io_configuration(void);

static configuration_t* config = NULL;
static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;
static char** destination_list = NULL;
static long sampling_rate = 10;

AIOConfiguration analogIOConfiguration;

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == NULL)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    const char* destinations = NULL;
    WriteInformation(logger, "Reading service configuration");
    destinations = configuration_get_value_as_string(config, "default", "destination");
    destination_list = strsplitchar(destinations, ',');
    sampling_rate = configuration_get_value_as_integer(config, "default", "sampling_rate");

    if(sampling_rate)
    {
        sampling_rate = 600;
    }

    WriteInformation(logger, "Reading analog IO configuration");
    read_analog_io_configuration();
    WriteInformation(logger, "Releasing configuration");
    configuration_release(config);

    WriteInformation(logger, "Initialising D to A");
    DacInit(logger);
    WriteInformation(logger, "Initialising A to D");
    Adc_init(logger);

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);
    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    bool continue_loop = true;

    while(continue_loop)
    {
        buffer_t* payload = buffer_allocate_default();

        for(int tChNum = 1 ; tChNum < analogIOConfiguration.channel_in_count+1 ; tChNum++)
        {
            float tEnggData = 0;
            short int tBinData = 0;

            tBinData = Adc_ReadData(logger, (ChannelNum_e)tChNum);

            if(tBinData == -1)
            {
                WriteLog(logger, "Could not read", LOG_ERROR);
                continue;
            }

            if(Adc_ConvertBin2Engg(logger, analogIOConfiguration.channels_in[tChNum].channel_type,
                                analogIOConfiguration.channels_in[tChNum].sys_high_cal,
                                analogIOConfiguration.channels_in[tChNum].sys_mid_cal,
                                analogIOConfiguration.channels_in[tChNum].sys_low_cal,
                                tBinData, &tEnggData) == -1)
            {
                continue;
            }

            buffer_append_string(payload, analogIOConfiguration.channels_in[tChNum].name);
            buffer_append_char(payload, ':');
            buffer_append_real(payload, tEnggData);
            buffer_append_char(payload, ',');

            sleep(1);
        }

        buffer_remove_end(payload, 1);


        for(int index = 0; destination_list[index] != NULL; index++)
        {
            if(!message_bus_send(message_bus, destination_list[index], Data, Text, (char*)buffer_get_data(payload), buffer_get_size(payload)))
            {
                continue_loop = false;
                break;
            }
            else
            {
                WriteInformation(logger, (const char*)buffer_get_data(payload));
            }
        }

        buffer_free(payload);

        sleep((unsigned int)sampling_rate);
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    strfreelist(destination_list);
    logger_release(logger);

    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char error_message[64];

        char** command_lines = strsplitchar(messagebuffer, '\n');

        char* command_name = command_lines[0];
        char* command_string = command_lines[1];

        char** command_values = strsplitchar(command_string, ',');

        for(int index = 0; command_values[index] != NULL; index++)
        {
            char** io_args = strsplitchar(command_values[index], ':');

            // String is composed as CHANNEL:VALUE
            int channel = atoi(io_args[0]);
            int value = atoi(io_args[1]);

            memset(error_message, 0, sizeof (error_message));

            if(strcmp(command_name, "READ") == 0)
            {
                buffer_t* payload = buffer_allocate_default();
                float tEnggData = 0;
                unsigned short int tBinData = 0;

                tBinData = Adc_ReadData(logger, (ChannelNum_e)channel);
                Adc_ConvertBin2Engg(logger, analogIOConfiguration.channels_in[channel].channel_type,
                                    analogIOConfiguration.channels_in[channel].sys_high_cal,
                                    analogIOConfiguration.channels_in[channel].sys_mid_cal,
                                    analogIOConfiguration.channels_in[channel].sys_low_cal,
                                    tBinData, &tEnggData);

                buffer_append_string(payload, analogIOConfiguration.channels_in[channel].name);
                buffer_append_char(payload, ':');
                buffer_append_real(payload, tEnggData);
                message_bus_send(message_bus, destination_list[index], Data, Text, (char*)buffer_get_data(payload), buffer_get_size(payload));
                buffer_free(payload);
            }
            else
            {
                if(strcmp(command_name, "WRITE") == 0)
                {
                    if(channel > 0 && channel < analogIOConfiguration.channel_out_count+1)
                    {
                        Dac_write(logger, channel, value);
                    }
                }
            }

            strfreelist(io_args);
        }

        strfreelist(command_lines);
        strfreelist(command_values);
    }
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            //WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            //WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            //WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            //WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            //WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            //WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            //WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            //WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}

void read_analog_io_configuration(void)
{
    analogIOConfiguration.channel_in_count = 0;
    analogIOConfiguration.channels_in = NULL;
    analogIOConfiguration.channel_out_count = 0;
    analogIOConfiguration.channels_out = NULL;

    char ptr[33] = {0};
    long index = 0;

    analogIOConfiguration.channel_in_count = configuration_get_value_as_integer(config, "aiCnfg", "NumOfChannels");

    if(analogIOConfiguration.channel_in_count > 1)
    {
        analogIOConfiguration.channels_in = (Channel*)calloc(analogIOConfiguration.channel_in_count, sizeof (Channel));
        for(index = 0; index < analogIOConfiguration.channel_in_count; index++)
        {
            memset(ptr, 0, 33);

            sprintf(ptr, "aiChannel-%ld", index+1);

            analogIOConfiguration.channels_in[index].enabled = configuration_get_value_as_boolean(config, ptr, "Enable");
            analogIOConfiguration.channels_in[index].channel_type = configuration_get_value_as_char(config, ptr, "ChannelType");
            analogIOConfiguration.channels_in[index].sys_low_cal = configuration_get_value_as_integer(config, ptr, "SysLowCal");
            analogIOConfiguration.channels_in[index].sys_mid_cal = configuration_get_value_as_integer(config, ptr, "SysMidcal");
            analogIOConfiguration.channels_in[index].sys_high_cal = configuration_get_value_as_integer(config, ptr, "SysHighCal");
            memset(analogIOConfiguration.channels_in[index].name, 0, 33);
            strncpy(analogIOConfiguration.channels_in[index].name, configuration_get_value_as_string(config, ptr, "Name"), 32);
            WriteInformation(logger, ptr);
            WriteInformation(logger, analogIOConfiguration.channels_in[index].name);
        }
    }

    analogIOConfiguration.channel_out_count = configuration_get_value_as_integer(config, "aoCnfg", "NumOfChannels");

    if(analogIOConfiguration.channel_out_count > 1)
    {
        analogIOConfiguration.channels_out = (Channel*)calloc(analogIOConfiguration.channel_out_count, sizeof (Channel));
        for(index = 0; index < analogIOConfiguration.channel_out_count; index++)
        {
            memset(ptr, 0, 33);

            sprintf(ptr, "aoChannel-%ld", index+1);

            analogIOConfiguration.channels_out[index].enabled = configuration_get_value_as_boolean(config, ptr, "Enable");
            analogIOConfiguration.channels_out[index].channel_type = configuration_get_value_as_char(config, ptr, "ChannelType");
            analogIOConfiguration.channels_out[index].sys_low_cal = configuration_get_value_as_integer(config, ptr, "SysLowCal");
            analogIOConfiguration.channels_out[index].sys_mid_cal = configuration_get_value_as_integer(config, ptr, "SysMidcal");
            analogIOConfiguration.channels_out[index].sys_high_cal = configuration_get_value_as_integer(config, ptr, "SysHighCal");
            memset(analogIOConfiguration.channels_out[index].name, 0, 33);
            strncpy(analogIOConfiguration.channels_out[index].name, configuration_get_value_as_string(config, ptr, "Name"), 32);
            WriteInformation(logger, ptr);
            WriteInformation(logger, analogIOConfiguration.channels_in[index].name);
        }
    }
}

