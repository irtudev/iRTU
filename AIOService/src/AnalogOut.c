//
// Created by mukeshp on 5/19/2020.
//
#include "AnalogOut.h"
#include "I2CInterface.h"

#define DAC_REG_ADDR_DEVICE_ID  (0x01)
#define DAC_REG_ADDR_SYNC       (0x02)
#define DAC_REG_ADDR_CONFIG     (0x03)
#define DAC_REG_ADDR_GAIN       (0x04)
#define DAC_REG_ADDR_TRIGGER    (0x05)
#define DAC_REG_ADDR_STATUS     (0x07)
#define DAC_REG_ADDR_DATA       (0x08)

static char error_string[64] = {0};

static int AnalogOutReadRegister(logger_t* logger, char iSlaveAddr, char iRegisterAddr, char *pBuf, unsigned int iBufLen);
static int AnalogOutWriteRegister(logger_t* logger, char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen);

int DacInit(logger_t *logger)
{
    DacReadDeviceIdentification(logger, AO1_I2C_SLAVE_ADDR);

    DacReadDeviceIdentification(logger, AO2_I2C_SLAVE_ADDR);

    DacWriteTriggerRegister(logger, AO1_I2C_SLAVE_ADDR, 0x000A);
    DacWriteTriggerRegister(logger, AO2_I2C_SLAVE_ADDR, 0x000A);
    sleep(5);

    DacGetGainAndDivisor(logger, AO1_I2C_SLAVE_ADDR, 0,0);
    DacGetGainAndDivisor(logger, AO2_I2C_SLAVE_ADDR, 0,0);

    /* 0 to 1.25 V output : Taken from TI support */
    //DacSetGainAndDivisor(AO1_I2C_SLAVE_ADDR, 0, 1);
    //DacSetGainAndDivisor(AO2_I2C_SLAVE_ADDR, 0, 1);

    /* 0 to 2.5 V output : Taken from TI support */
    DacSetGainAndDivisor(logger, AO1_I2C_SLAVE_ADDR, 1, 1);
    DacSetGainAndDivisor(logger, AO2_I2C_SLAVE_ADDR, 1, 1);

    DacGetGainAndDivisor(logger, AO1_I2C_SLAVE_ADDR, 0,0);
    DacGetGainAndDivisor(logger, AO2_I2C_SLAVE_ADDR, 0,0);

    return 0;
}

int Dac_write(logger_t *logger, char iDevice, unsigned short int iData)
{
    /** 10V - 0xFFF
     *  0V  - 0x000
     *  5V  - 0x7FF
     * */

    unsigned short int tDataRead1 = 0;
    unsigned short int tDataRead2 = 0;

    DacWriteData(logger, iDevice, iData);
    //DacWriteData(AO2_I2C_SLAVE_ADDR, iData);

    DacReadData(logger, iDevice, &tDataRead1);
    //DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    memset(error_string, 0, 64);
    sprintf(error_string, "tDataRead:%x", tDataRead1);
    WriteInformation(logger, error_string);
    //printf("tDataRead2:%x\n", tDataRead2);

    //DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    //DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    //printf("tDataRead1-Status:%x\n", tDataRead1);
    //printf("tDataRead2-Status:%x\n", tDataRead2);

#if 0
    DacWriteData(AO1_I2C_SLAVE_ADDR, 0x7FF);
        DacWriteData(AO2_I2C_SLAVE_ADDR, 0x7FF);

        DacReadData(AO1_I2C_SLAVE_ADDR, &tDataRead1);
        DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
        printf("tDataRead1:%x\n", tDataRead1);
        printf("tDataRead2:%x\n", tDataRead2);

        DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
        DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
        printf("tDataRead1-Status:%x\n", tDataRead1);
        printf("tDataRead2-Status:%x\n", tDataRead2);
        sleep(10);
#endif
#if 0
    DacWriteData(AO1_I2C_SLAVE_ADDR, 0x000);
    DacWriteData(AO2_I2C_SLAVE_ADDR, 0xFFF);

    DacReadData(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1:%x\n", tDataRead1);
    printf("tDataRead2:%x\n", tDataRead2);

    DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1-Status:%x\n", tDataRead1);
    printf("tDataRead2-Status:%x\n", tDataRead2);

    DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1-Status:%x\n", tDataRead1);
    printf("tDataRead2-Status:%x\n", tDataRead2);

    sleep(10);
#endif
    return 0;
}

/*
 *
 */
int DacReadDeviceIdentification(logger_t *logger, char iSlaveAddr)
{
    char tDeviceIdBuf[2] = {0,};
    //unsigned short int tDeviceId = 0;

    if(0 != AnalogOutReadRegister(logger, iSlaveAddr, DAC_REG_ADDR_DEVICE_ID, (char*)&tDeviceIdBuf, 2))
    {
        WriteLog(logger, "AnalogOut Device Id read failed", LOG_ERROR);
        return -1;
    }

    memset(error_string, 0, 64);
    sprintf(error_string, "tDeviceId[0]:%x, tDeviceId[1]:%x", tDeviceIdBuf[0], tDeviceIdBuf[1]);
    WriteInformation(logger, error_string);

    return 0;
}

/* It set DAC mode of operations: synchronpus or asynchronous
 * Synchronous Mode: When set to 1, the DAC output is set to update in response to an LDAC trigger (synchronous mode).
 * Asynchronous Mode: When cleared to 0 ,the DAC output is set to update immediately (asynchronous mode), default.
 * */
int DacSetOperationMode(logger_t *logger, char iSlaveAddr, unsigned short int iMode)
{
    //unsigned short int tMode = 0x01; //0x01 /* Synchronous*/ , 0x00 /* Asynchronous mode*/;
    if(0 != AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_SYNC, (char*)&iMode, 2))
    {
         WriteLog(logger, "Error to set operational mode", LOG_ERROR);
        return -1;
    }

    return 0;
}

/*
 *
 */
int DacSoftReset(logger_t *logger, char iSlaveAddr)
{
    unsigned short int tSoftResetCmd = 0x000A;

    if(0!= AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)&tSoftResetCmd, 2))
    {
         WriteLog(logger, "Dac soft reset failed", LOG_ERROR);
        return -1;
    }

    return 0;
}

/*
 *
 */
int DacLoadSynchronously(logger_t *logger, char iSlaveAddr)
{
    unsigned short int tSyncLoadCmd = 0x0010;

    if(0!= AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)&tSyncLoadCmd, 2))
    {
         WriteLog(logger, "Dac synchronously load failed", LOG_ERROR);
        return -1;
    }

    return 0;
}

/**
 * Select Reference voltage.
 * 1 : External reference voltage, 0: Internal reference voltage.
 * @return
 */
int DcaSelectReferenceVoltage(logger_t *logger, char iSlaveAddr, char iRef)
{
    unsigned short int tRefVolData = 0;

    if(1 == iRef) /* External reference voltage */
    {
        tRefVolData = 0x0100;
    }
    else if(0 == iRef) /* Internal reference voltage */
    {
        tRefVolData = 0;
    }
    else
    {
        WriteLog(logger, "Invalid reference voltage selection", LOG_ERROR);
        return -1;
    }

    if(0!= AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_CONFIG, (char*)&tRefVolData, 2))
    {
        WriteLog(logger, "Dac reference voltage selection failed", LOG_ERROR);
        return -1;
    }

    return 0;
}

int DacSetGainAndDivisor(logger_t *logger, char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv)
{
    unsigned short int tMulAndDiv = 0;

    if(1 == iBufGain) /* When set to 1, the buffer amplifier for corresponding DAC has a gain of 2.*/
    {
        tMulAndDiv |= 0x0001;
    }
    else if(0 == iBufGain) /* When clear to 0, the buffer amplifier for corresponding DAC has a gain of 1.*/
    {
        tMulAndDiv &= (~0x0001);
    }
    else
    {
        WriteLog(logger, "Invalid multiplier value", LOG_ERROR);
    }

    if(1 == iRefDiv) /* When set to 1, the reference voltage division by 2 */
    {
        tMulAndDiv |= 0x0100;
    }
    else if(0 == iRefDiv) /* When clear to 0, the reference voltage division by 1 */
    {
        tMulAndDiv &= (~0x0100);
    }
    else
    {
        WriteLog(logger, "Invalid Divisor value", LOG_ERROR);
    }

    char tBuffer[8] = {0,};

    tBuffer[0] = (char)(tMulAndDiv >> 8) & 0x00FF;
    tBuffer[1] = tMulAndDiv & 0x00FF;

    if(0 != AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_GAIN, tBuffer, 2))
    {
        WriteLog(logger, "Dac set multiplier and divisor failed", LOG_ERROR);
        return -1;
    }

    return 0;
}

int DacGetGainAndDivisor(logger_t *logger, char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv)
{
    char tBuffer[8] = {0,};

    if(0 != AnalogOutReadRegister(logger, iSlaveAddr, DAC_REG_ADDR_GAIN, (char*)tBuffer, 2))
    {
        WriteLog(logger, "Dac Get multiplier and divisor failed", LOG_ERROR);
        return -1;
    }

    memset(error_string, 0, 64);
    sprintf(error_string, "Gain Register -> tBuffer[1]:%x, tBuffer[0]:%x", tBuffer[1], tBuffer[0]);
    WriteLog(logger, error_string, LOG_INFO);
    return 0;
}

/*
 *
 */
int DacWriteData(logger_t *logger, char iSlaveAddr, unsigned short int iData)
{
    char tBuffer[8] = {0,};

    //iData = (iData << 8) & 0xFFF0;
    //iData &= 0x0FFF;

    tBuffer[0] = (char)((iData & 0x0FF0) >> 4);
    tBuffer[1] = (char)((iData & 0x000F) << 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "Data Write tBuffer[0]:%x, tBuffer[1]:%x", tBuffer[0], tBuffer[1]);
    WriteLog(logger, error_string, LOG_INFO);

    //if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_DATA, (char*)&iData, 2))
    if(0!= AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_DATA, (char*)tBuffer, 2))
    {
        WriteLog(logger, "Dac Data write failed", LOG_ERROR);
        return -1;
    }

    return 0;
}

int DacReadData(logger_t *logger, char iSlaveAddr, unsigned short int *pData)
{
    char tBuffer[8] = {0,};

    if(0!= AnalogOutReadRegister(logger, iSlaveAddr, DAC_REG_ADDR_DATA, (char*)tBuffer, 2))
    {
        WriteLog(logger, "Dac Data read failed", LOG_ERROR);
        return -1;
    }

    memset(error_string, 0, 64);
    sprintf(error_string, "tBuffer[0]:%x,tBuffer[1]:%x", tBuffer[0], tBuffer[1]);
    WriteLog(logger, error_string, LOG_INFO);

    (*pData) = 0;
    (*pData) = (tBuffer[0] << 8);
    (*pData) = (*pData) | tBuffer[1];

    return 0;
}

int DacReadStatusRegister(logger_t *logger, char iSlaveAddr, unsigned short int *pData)
{
    char tBuffer[8] = {0,};

    if(0!= AnalogOutReadRegister(logger, iSlaveAddr, DAC_REG_ADDR_STATUS, (char*)tBuffer, 2))
    {
       WriteLog(logger, "Dac Status register read failed", LOG_ERROR);
        return -1;
    }

    (*pData) = 0;
    (*pData) = (tBuffer[0] << 8);
    (*pData) = (*pData) | tBuffer[1];

    return 0;
}

int DacWriteTriggerRegister(logger_t *logger, char iSlaveAddr, unsigned short int iData)
{
    char tBuffer[8] = {0,};

    iData &= 0x0FFF;

    tBuffer[0] = (char)((iData & 0xFF00)>>8);
    tBuffer[1] = (char)(iData & 0x00FF);

    if(0!= AnalogOutWriteRegister(logger, iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)tBuffer, 2))
    {
        WriteLog(logger, "Dac Trigger register write failed", LOG_ERROR);
        return -1;
    }

    return 0;
}


static int AnalogOutWriteRegister(logger_t *logger, char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen)
{
    char tDeviceName[64] = {0,};
    int tFd = -1;
    int tRetVal = -1;
    char tBuffer[16] = {0,};

    tBuffer[0] = iRegisterAddr;
    tBuffer[1] = pDataBuf[0];
    tBuffer[2] = pDataBuf[1];
    tBuffer[3] = '\0';

#if 0
    /** todo: Need to identify adapter number with sting operation from /sys/class/i2c-dev/ */
    if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-0");
    }
    else if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-1");
    }
#endif

    strcpy(tDeviceName, "/dev/i2c-1");

    tFd = I2C_Open(logger, tDeviceName);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at I2c_Device open (%s)", tDeviceName);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            WriteLog(logger, "Error to set slave address", LOG_ERROR);
            //exit(1);
            break;
        }

#if 0
        if (1 != I2C_Write(tFd, tBuffer, 1)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to write register address at %s\n", __func__);
            //exit(1);
            break;
        }
#endif
        if (3 != I2C_Write(logger, tFd, tBuffer, 3)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            WriteLog(logger, "Error to write register address", LOG_ERROR);
            //exit(1);
            break;
        }

        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(logger, tFd);

    return tRetVal;
}

static int AnalogOutReadRegister(logger_t *logger, char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen)
{
    extern int adaptorNo;
    char tDeviceName[64] = {0,};
    int tFd = -1;
    int tRetVal = -1;

    /** todo: Need to identify adapter number with sting operation from /sys/class/i2c-dev/ */
#if 0
    if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-0");
    }
    else if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-1");
    }
#endif

    //sprintf(tDeviceName,"/dev/i2c-%d", adaptorNo);
    strcpy(tDeviceName, "/dev/i2c-1");

    tFd = I2C_Open(logger, tDeviceName);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at I2c_Device open(%s)", tDeviceName);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            WriteLog(logger, "Error to set slave address", LOG_ERROR);
            //exit(1);
            break;
        }

        if (1 != I2C_Write(logger, tFd, &iRegisterAddr, 1)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            WriteLog(logger, "Error to write register address", LOG_ERROR);
            //exit(1);
            break;
        }

        if (iDataBufLen != I2C_Read(logger, tFd, pDataBuf, iDataBufLen)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            WriteLog(logger, "Error to write register address", LOG_ERROR);
            //exit(1);
            break;
        }

        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(logger, tFd);

    return tRetVal;
}

