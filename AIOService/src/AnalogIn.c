//
// Created by mukeshp on 5/21/2020.
//

#include "AnalogIn.h"
#include "SPIInterface.h"

//extern AiCnfg_t    global_analog_in_configuration;
static char error_string[64] = {0};

int Adc_init(logger_t *logger)
{
    unsigned long int tDeviceId;
    unsigned long int tDataOutGetMode;
    unsigned long int tSdiMode;
    unsigned long int tSdoMode;
    unsigned long int tGetRangeSelect;
    unsigned long int tGetAlarmHighThresold;
    unsigned long int tGetAlarmLowThresold;
    unsigned long int tGetPowerCntlReg;

    /* ADC : Channel selection pin configure */
    Adc_IOPinConfigure(logger);

    /* ADC : Interrupt pin configure */
    Adc_InterruptPinConfigure(logger);

    /* ADC : Get Device Id */
    AdcGetDeviceIdentification(logger, &tDeviceId);

    /* ADC : Set output conversion data mode  */
    //AdcSetDataOutMode(DEVICE_ADDR_INCL | SELECTED_RANGE_INCL | PARITY_EN);
    AdcSetDataOutMode(logger, 0);
    AdcGetDataOutMode(logger, &tDataOutGetMode);

    /* ADC : Set SDI mode */
    AdcSetSdiMode(logger, tSdiMode);
    AdcGetSdiMode(logger, &tSdiMode);

    /* ADC : Set SDO mode */
    AdcSetSdoMode(logger, tSdiMode);
    AdcGetSdoMode(logger, &tSdoMode);

    /* ADC : Set reference voltage, and input voltage range selection */
    AdcSetRangeReferenceVoltage(logger, INT_REF_VOLTAGE, VOLTAGE_RANGE_BIPOLAR_2_5V);
    //AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_UNIPOLAR_1_25V);
    AdcGetRangeReferenceVoltage(logger, &tGetRangeSelect);

    /* Misc */
    AdcGetAlarmHighThresoldRegister(logger, &tGetAlarmHighThresold);

    AdcGetAlarmLowThresoldRegister(logger, &tGetAlarmLowThresold);

    AdcGetResetPowerCntrlReg(logger, &tGetPowerCntlReg);

    return 0;
}

/**
 *
 * */
int AdcGetDeviceIdentification(logger_t* logger, unsigned long *pDeviceId)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    if(NULL == pDeviceId)
    {
        WriteLog(logger, "DeviceId is NULL", LOG_ERROR);
        return -1;
    }

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DEVICE_ID_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4,(unsigned char *) &tRdData[0], 2) < 0) {
            WriteLog(logger, "Error SpiRead", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DEVICE_ID_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4,(unsigned char *) &tRdData[2], 2) < 0) {
            memset(error_string, 0, 64);
            WriteLog(logger, "Error SpiRead", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pDeviceId, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "Device Id: 0x%08lX", (*pDeviceId));
    WriteLog(logger, error_string, LOG_INFO);

    return (tRetVal);
}

/** AdcRangeReferenceVoltageSelection
 * iRefSel : 0 - Internal reference voltage, 1 - External reference voltage.
 * iRangeVoltage :  0000b (0x00) = ±3 × VREF
 *                  0001b (0x01) = ±2.5 × VREF
 *                  0010b (0x02) = ±1.5 × VREF
 *                  0011b (0x03) = ±1.25 × VREF
 *                  0100b (0x04) = ±0.625 × VREF
 *                  1000b (0x08) = 3 × VREF
 *                  1001b (0x09) = 2.5 × VREF
 *                  1010b (0x0A) = 1.5 × VREF
 *                  1011b (0x0B) = 1.25 × VREF
 * */
int AdcSetRangeReferenceVoltage(logger_t *logger, unsigned char iRefSel, unsigned char iRangeVoltage)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned short int tData = 0;
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_INFO);
        return -1;
    }

    do
    {
        tData = (iRefSel << 6) | iRangeVoltage;

        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_RANGE_SEL_REG;
        tWrData[2] = (unsigned char) (tData >> 8);
        tWrData[3] = (unsigned char) tData;

        if (SpiWrite(logger, tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            WriteLog(logger, "Error to SpiWrite-Reg Address", LOG_INFO);
            tRetVal = -1;
            break;
        }
    }while (0);

    SpiClose(logger, tFd);

    return (tRetVal);
}

int AdcGetRangeReferenceVoltage(logger_t *logger, unsigned long *pRangeSel)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do {
        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_RANGE_SEL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0) {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_RANGE_SEL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0) {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pRangeSel, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "pRangeSel: 0x%08lX", (*pRangeSel));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

int AdcGetAlarmHighThresoldRegister(logger_t *logger, unsigned long *pAlarmHighThresold)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_H_TH_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_H_TH_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pAlarmHighThresold, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "pAlarmHighThresold: 0x%08lX", (*pAlarmHighThresold));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

int AdcGetAlarmLowThresoldRegister(logger_t *logger, unsigned long *pAlarmLowThresold)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do {

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_L_TH_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0) {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_L_TH_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0) {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pAlarmLowThresold, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "pAlarmLowThresold: 0x%08lX", (*pAlarmLowThresold));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

int AdcGetResetPowerCntrlReg(logger_t *logger, unsigned long *pPowerCntrlreg)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    if(NULL == pPowerCntrlreg)
    {
        WriteLog(logger, "PowerCntrlreg is NULL", LOG_ERROR);
        return -1;
    }

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_RST_PWRCTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, (unsigned char *) &tRdData[0], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[2]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_RST_PWRCTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, (unsigned char *) &tRdData[2], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pPowerCntrlreg, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "RST Power CNTL REG: 0x%08lX", (*pPowerCntrlreg));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

/* iSdiMode :   0 - Standard SPI with CPOL = 0 and CPHASE = 0
 *              1 - Standard SPI with CPOL = 0 and CPHASE = 1
 *              2 - Standard SPI with CPOL = 1 and CPHASE = 0
 *              3 - Standard SPI with CPOL = 1 and CPHASE = 1
 * */
int AdcSetSdiMode(logger_t *logger, unsigned char iSdiMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = 0;
    unsigned char tWrData[4] = {0, };

    tData = iSdiMode;

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(logger, tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

    }while (0);

    SpiClose(logger, tFd);

    return (tRetVal);
}

int AdcGetSdiMode(logger_t *logger, unsigned long *pSdiMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(logger, tFd);

    memcpy(pSdiMode, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "SDI MODE: %lX", (*pSdiMode));
    WriteInformation(logger, error_string);

    return 0;
}

int AdcGetSdoMode(logger_t *logger, unsigned long *pSdoMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDO_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDO_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3])

    }while (0);

    SpiClose(logger,tFd);

    memcpy(pSdoMode, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "SDO MODE: %lX", (*pSdoMode));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

int AdcSetSdoMode(logger_t *logger, unsigned char iSdoMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = 0;
    unsigned char tWrData[4] = {0, };

    tData = iSdoMode;

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1 ;
        tWrData[1] = ADS8689_SDO_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(logger, tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

    }while (0);

    SpiClose(logger, tFd);

    return (tRetVal);
}

int AdcSetDataOutMode(logger_t *logger, unsigned int iDataOutMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = iDataOutMode;
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(logger, tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

    }while(0);

    SpiClose(logger, tFd);

    return (tRetVal);
}

int AdcGetDataOutMode(logger_t *logger, unsigned long int *pDataOutMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to SpiOpen(%s)", ANALOG_IN_DEVICE);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(logger, tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            WriteLog(logger, "Error at SpiRead-Reg", LOG_ERROR);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);
    }
    while (0);

    SpiClose(logger, tFd);

    memcpy(pDataOutMode, &tRdData, 4);

    memset(error_string, 0, 64);
    sprintf(error_string, "DATA_OUT MODE: 0x%08lX", (*pDataOutMode));
    WriteInformation(logger, error_string);

    return (tRetVal);
}

int AdcChannelSelection(logger_t *logger, ChannelNum_e iChNum)
{
    #if 0
    if((iChNum >= CHANNEL_NUM_1) && (iChNum <= CHANNEL_NUM_4))
    {
        /* Channel Range selection : Current channel : 0 to 5.12 V, Voltage channel: -10V to +10V */
        if(gAiCnfg.mAiChannelCnfg[iChNum].mChannelType == AI_CHANNEL_TYPE_BIPOLAR_10V)
        {
            AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_BIPOLAR_2_5V);
        }
        else
        {
            AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_UNIPOLAR_1_25V);
        }
    }
    #endif

    switch (iChNum)
    {
        case CHANNEL_NUM_1:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_2:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_3:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_4:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_DISABLE_ALL:
        {
            /* EN_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio57/value");
        }
        default:
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Invalid analog channel number(%d)", iChNum);
            WriteLog(logger, error_string, LOG_ERROR);
        }
    }

    usleep(50000); /* 50ms Delay after channel selection */
    return 0;
}

#if 0
void* AdcRvsInterrupt_Thread(void *arg)
{
    char gpio_irq[64];
    int ret, irqfd = 0, i = 0;
    fd_set fds;
    FD_ZERO(&fds);
    int buf;

    pthread_detach(pthread_self());


    snprintf(gpio_irq, sizeof(gpio_irq), "/sys/class/gpio/gpio%d/value", 49);
    irqfd = open(gpio_irq, O_RDONLY, S_IREAD);

    if(irqfd == -1) {
        printf("Could not open IRQ %s\n", 49);
        printf("Make sure the GPIO is already exported", 49);
        return NULL;
    }

    // Read first since there is always an initial status
    ret = read(irqfd, &buf, sizeof(buf));

    while(1) {
        FD_SET(irqfd, &fds);
        // See if the IRQ has any data available to read
        ret = select(irqfd + 1, NULL, NULL, &fds, NULL);

        if(FD_ISSET(irqfd, &fds))
        {
            FD_CLR(irqfd, &fds);  //Remove the filedes from set
            //printf("IRQ detected %d\n", i);
            fflush(stdout);
            i++;

            /* The return value includes the actual GPIO register value */
            read(irqfd, &buf, sizeof(buf));
            lseek(irqfd, 0, SEEK_SET);
        }

        //Sleep, or do any other processing here
        //usleep(100000);
    }

    pthread_exit(NULL);
}
#endif

void Adc_IOPinConfigure(logger_t *logger)
{
    int tFdEnMux = -1;
    int tFdA0Mux = -1;
    int tFdA1Mux = -1;

    tFdEnMux = open("/sys/class/gpio/gpio57/direction", O_RDWR);
    if (tFdEnMux < 0)
    {
        //if not exported already than export it
        system("echo \"57\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdEnMux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio57/direction");

    tFdA0Mux = open("/sys/class/gpio/gpio191/direction", O_RDWR);
    if (tFdA0Mux < 0)
    {
        //if not exported already than export it
        system("echo \"191\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdA0Mux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio191/direction");

    tFdA1Mux = open("/sys/class/gpio/gpio52/direction", O_RDWR);
    if (tFdA1Mux < 0)
    {
        //if not exported already than export it
        system("echo \"52\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdA1Mux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio52/direction");

    return;
}

void Adc_InterruptPinConfigure(logger_t *logger)
{
    int tFdAdcRvs = -1;
    char tgpioirq[256] = {0,};
    char tCommand[256] = {0,};

    sprintf(tgpioirq, "/sys/class/gpio/%s/direction", GPIO_ADC_RVS_STR);

    tFdAdcRvs = open(tgpioirq, O_RDWR);
    if (tFdAdcRvs < 0)
    {
        //if not exported already than export it
        system("echo \"49\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdAdcRvs);
    }

    sprintf(tCommand, "echo \"in\" > /sys/class/gpio/%s/direction", GPIO_ADC_RVS_STR);
    system(tCommand);

    sprintf(tCommand,"echo \"rising\" > /sys/class/gpio/%s/edge", GPIO_ADC_RVS_STR);
    system(tCommand);

    return;
}

short int Adc_ReadData(logger_t* logger, ChannelNum_e iChannelNum)
{
    char gpio_irq[64] = {0, };
    int irqfd = 0;
    fd_set fds;
    int buf;
    unsigned char tDummyWrData[4] = {0, };
    unsigned long int tAverageData = 0;
    unsigned short int tConvData[ADC_AVERAGE_CNT] = {0, };
    unsigned long int tCumulativeData = 0;

    /** Channel Selection */
    AdcChannelSelection(logger, iChannelNum);

    snprintf(gpio_irq, sizeof(gpio_irq), "/sys/class/gpio/%s/value", GPIO_ADC_RVS_STR);
    irqfd = open(gpio_irq, O_RDONLY, S_IREAD);
    if(irqfd == -1)
    {
        memset(error_string, 0, 64);
        sprintf(error_string, "Can's open ADC_IRQ (%s), export GPIO first", GPIO_ADC_RVS_STR);
        WriteLog(logger, error_string, LOG_ERROR);
        return -1;
    }

    FD_ZERO(&fds);

    for(int tcnt = 0 ; tcnt < ADC_AVERAGE_CNT ; ++tcnt)
    {
        FD_SET(irqfd, &fds);

        /* See if the IRQ has any data available to read */
        select(irqfd + 1, NULL, NULL, &fds, NULL);

        if (FD_ISSET(irqfd, &fds))
        {
            /* Remove the file des from set */
            FD_CLR(irqfd, &fds);

            /* The return value includes the actual GPIO register value */
            read(irqfd, &buf, sizeof(buf));
            lseek(irqfd, 0, SEEK_SET);

            unsigned char tConvertedData[4] = {0,};
            int tFd = SpiOpen(logger, ANALOG_IN_DEVICE);
            if (tFd < 0)
            {
                memset(error_string, 0, 64);
                printf("Error to open %s", ANALOG_IN_DEVICE);
            }

            SpiRead(logger, tFd, (unsigned char *) tDummyWrData, 4, tConvertedData, 4);
            SpiClose(logger, tFd);

            //tConvData[tcnt] = (tConvertedData[0] << 24) | (tConvertedData[1] << 16) || (tConvertedData[2] << 8) || tConvertedData[3];
            tConvData[tcnt] = (tConvertedData[0] << 8) | (tConvertedData[1]) ;
            //printf("tConvData[%d]:0x%04X\n", tcnt+1, tConvData[tcnt]);
        }
    }

    tCumulativeData = 0;
    for(int i = 0 ; i < ADC_AVERAGE_CNT ; ++i)
    {
        tCumulativeData += tConvData[i] ;
        //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    }

    //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    tAverageData = tCumulativeData / ADC_AVERAGE_CNT;
    //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    //printf("tAverageData : 0x%04X\n", tAverageData);

    return (tAverageData);
}

int Adc_ConvertBin2Engg(logger_t *logger, char chtype, long highcal, long midcal, long lowcal, unsigned short int tAnalogDataBin, float *pAnalogDataEngg)
{
    switch(chtype)
    {
        case 'I':
        {
            if(tAnalogDataBin >= midcal)
            {
                (*pAnalogDataEngg) = 10 + ((float)((tAnalogDataBin - midcal) * ((20-0)/2) ) /
                                          (highcal - midcal));
            }
            else if(tAnalogDataBin < midcal)
            {
                (*pAnalogDataEngg) = 0 + ((float)((tAnalogDataBin - midcal) * ((20-0)/2) ) /
                                          (midcal - lowcal));
            }
            return  0;
        }
        case 'V':
        {
            if(tAnalogDataBin > midcal)
            {
                (*pAnalogDataEngg) = ((float)((tAnalogDataBin - midcal) * (10-0))) /
                                     (highcal - midcal);
            }
            else if(tAnalogDataBin < midcal)
            {
                (*pAnalogDataEngg) = 0 - (((float)((midcal - tAnalogDataBin) * (10-0))) /
                                          (midcal - lowcal));
            }
            else
            {
                (*pAnalogDataEngg) = 0;
            }
            return 0;
        }
        default:
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Invalid channel:%u type:%c", (unsigned short)lowcal, chtype);
            WriteInformation(logger, error_string);
        }
    }

    return -1;
}
