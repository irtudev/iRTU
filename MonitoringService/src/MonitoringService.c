#include "MonitoringService.h"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>
#include <unistd.h>
#include <linux/sysctl.h>
#include <sys/syscall.h>
#include <libudev.h>

#include <RTUCore/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/StringList.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>
#include <RTUCore/Dictionary.h>
#include <RTUCore/Buffer.h>

static void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void on_signal_received(SignalType type);

static message_bus_t* message_bus = NULL;
static logger_t* logger = NULL;

#define I2C_DEVICE_NAME     "/dev/i2c-1"

bool service_initialize()
{
    logger = logger_allocate_default();

    if(logger == NULL)
    {
        return false;
    }

    signals_register_callback(on_signal_received);
    signals_initialize_handlers();

    return true;
}

bool service_destroy()
{
    return true;
}

bool service_start()
{
    message_bus = message_bus_initialize(on_network_event);

    if(!message_bus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    bool continue_loop = true;

    while(continue_loop)
    {
        buffer_t* payload = buffer_allocate_default();

        buffer_append_string(payload, "timestamp");
        buffer_append_char(payload, ':');
        buffer_append_curr_timestamp(payload);

        if(message_bus_send(message_bus, "service_transport", Data, UserData, Text, (char*)buffer_get_data(payload), buffer_get_size(payload)))
        {
            WriteInformation(logger, (char*)buffer_get_data(payload));
        }

        buffer_free(payload);

        sleep(10);
    }

    return true;
}

bool service_restart()
{
    return false;
}

bool service_stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    logger_release(logger);
    return false;
}

void on_network_event(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{  
    char str[64] = {0};

    if(mtype == Register)
    {
        sprintf(str, "%s is online\n", messagebuffer);
        WriteInformation(logger, str);
        return;
    }

    if(mtype == DeRegister)
    {
        sprintf(str, "%s is offline\n", messagebuffer);
        WriteInformation(logger, str);
        return;
    }

    if(mtype == LoopBack)
    {
        WriteInformation(logger,"Loopback test successfull");
        return;
    }

    WriteInformation(logger,messagebuffer);
}

void on_signal_received(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            service_stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
