//
// Created by mukeshp on 5/19/2020.
//
#include "I2CInterface.h"

static char error_string[64] = {0};

static inline __s32 i2c_smbus_access(int file, char read_write, __u8 command,
                                     int size, union i2c_smbus_data *data)
{
    struct i2c_smbus_ioctl_data args;

    args.read_write = read_write;
    args.command = command;
    args.size = size;
    args.data = data;
    return ioctl(file,I2C_SMBUS,&args);
}


static inline __s32 i2c_smbus_read_byte_data(int file, __u8 command)
{
    union i2c_smbus_data data;
    if (i2c_smbus_access(file,I2C_SMBUS_READ,command,
                         I2C_SMBUS_BYTE_DATA,&data))
        return -1;
    else
        return 0x0FF & data.byte;
}

int I2C_WriteRegister(logger_t* logger, char *pDeviceName, char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen)
{
    int tFd = -1;
    int tIdx = 0;
    int tRetVal = -1;
    char tBuffer[16] = {0,};

    tBuffer[tIdx++] = iRegisterAddr;

    for( int temp = 0; temp < iDataBufLen ; ++temp)
    {
        tBuffer[tIdx++] = pDataBuf[temp];
    }

    tBuffer[tIdx] = '\0';

    #if 0
    for(int i=0; i< (iDataBufLen+1) ; ++i)
    {
        printf("tBuffer[%d]:0x%X, ", i, tBuffer[i]);
    }
    printf("\n");
    #endif

    tFd = I2C_Open(logger, pDeviceName);
    if(tFd < 0)
    {
        return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0)
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Error to set slave address");
            WriteLog(logger, error_string, LOG_ERROR);
            break;
        }

        if ((iDataBufLen+1) != I2C_Write(logger, tFd, tBuffer, iDataBufLen+1))
        {
            break;
        }

        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(logger, tFd);

    return tRetVal;
}

int I2C_ReadRegister(logger_t *logger, char *pDeviceName, char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen, char iFlag)
{
    int tFd = -1;
    int tRetVal = -1;

    tFd = I2C_Open(logger, pDeviceName);

    if(tFd < 0)
    {
         return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0)
        {
            memset(error_string, 0, 64);
            sprintf(error_string, "Error to set slave address");
            WriteLog(logger, error_string, LOG_ERROR);
            break;
        }

        if(iFlag == 0)
        {
            if (1 != I2C_Write(logger, tFd, &iRegisterAddr, 1))
            {
                break;
            }

            sleep(1);

            if (iDataBufLen != I2C_Read(logger, tFd, pDataBuf, iDataBufLen))
            {
                break;
            }
        }
        else
        {
            pDataBuf[0] = i2c_smbus_read_byte_data(tFd, iRegisterAddr);
        }
        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(logger, tFd);

    return tRetVal;
}

int I2C_Open(logger_t *logger, char *pDeviceName)
{
    int tFd = -1 ;
    int adapter_nr = 2; /* probably dynamically determined */
    char tFileName[64] = {0,};

    tFd = open (pDeviceName, O_RDWR);
    if (tFd < 0)
    {
        /* ERROR HANDLING; you can check errno to see what went wrong */
        memset(error_string, 0, 64);
        sprintf(error_string, "Error to open %s device at I2C_Open", pDeviceName);
        WriteLog(logger, error_string, LOG_ERROR);
        //exit(1);
        return -1;
    }

    return tFd;
}

int I2C_Write(logger_t* logger, int iFd, char *pBuf, unsigned int iBufLen)
{
    int tWrittenBytes = -1;

    tWrittenBytes = write(iFd, pBuf, iBufLen);
    if(tWrittenBytes < 0)
    {
        /* ERROR HANDLING: i2c transaction failed */
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at I2C_Write(%s)", strerror(errno));
        WriteLog(logger, error_string, LOG_ERROR);
    }
    else
    {
        //printf("Success at I2C_Write\n");
    }

    return tWrittenBytes;
}

int I2C_Read(logger_t* logger, int iFd, char *pBuf, unsigned int iBufLen)
{
    int tReadBytes = -1;

    /* Using I2C Read */
    tReadBytes = read(iFd, pBuf, iBufLen);
    if(tReadBytes < 0)
    {
        /* ERROR HANDLING: i2c transaction failed */
        memset(error_string, 0, 64);
        sprintf(error_string, "Error at I2C_Read(%s)", strerror(errno));
        WriteLog(logger, error_string, LOG_ERROR);
    }
    else
    {
        /* buf[0] contains the read byte */
        //printf("I2C Read success \n");
    }

    return tReadBytes;
}

void I2C_Close(logger_t* logger, int iFd)
{
    close(iFd);
    //printf("I2C Close executed\n");
}
