#ifndef PERIPHERAL_SIMULATOR
#define PERIPHERAL_SIMULATOR

#include <stdbool.h>

bool service_initialize();
bool service_destroy();
bool service_start();
bool service_restart();
bool service_stop();

#endif
