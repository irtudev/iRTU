cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(APP_VERSION_MAJOR 1)
set(APP_VERSION_MINOR 0)
set(APP_VERSION_PATCH 0)

set(Project MonitoringService)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore udev)

set(SOURCE
${SOURCE}
./src/MonitoringService.c
./src/I2CInterface.c
./src/PressureSensor.c
./src/HumiditySensor.c
./src/Accelerometer.c
./src/main.c
)

set(HEADERS
${HEADERS}
./include/MonitoringService.h
./include/I2CInterface.h
./include/PressureSensor.h
./include/HumiditySensor.h
./include/Accelerometer.h
)

add_executable(service_monitor ${SOURCE} ${HEADERS})

install(TARGETS service_monitor DESTINATION /usr/bin)
set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_PATCH})

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Subrato Roy")
set(CPACK_PACKAGE_DESCRIPTION, "Monitoring Service")

include(CPack)

